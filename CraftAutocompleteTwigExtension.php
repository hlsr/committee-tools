<?php
/**
 * CraftAutocompleteTwigExtension
 *
 * This is intended to be used in a Craft 3.x project, with the Symfony Plugin for PhpStorm:
 * @see https://plugins.jetbrains.com/plugin/7219-symfony-plugin
 *
 * @see https://nystudio107.com/blog/auto-complete-craft-cms-3-apis-in-twig-with-phpstor
 * @see https://github.com/Haehnchen/idea-php-symfony2-plugin/issues/1103
 */

namespace topshelfcraft\project;

/**
 * @property \craft\web\twig\variables\Cp $cp
 * @property \craft\web\twig\variables\Io $io
 * @property \craft\web\twig\variables\Routes $routes
 * @property \craft\web\twig\variables\CategoryGroups $categoryGroups
 * @property \craft\web\twig\variables\Config $config
 * @property \craft\web\twig\variables\Deprecator $deprecator
 * @property \craft\web\twig\variables\ElementIndexes $elementIndexes
 * @property \craft\web\twig\variables\EntryRevisions $entryRevisions
 * @property \craft\web\twig\variables\Feeds $feeds
 * @property \craft\web\twig\variables\Fields $fields
 * @property \craft\web\twig\variables\Globals $globals
 * @property \craft\web\twig\variables\I18N $i18n
 * @property \craft\web\twig\variables\Request $request
 * @property \craft\web\twig\variables\Sections $sections
 * @property \craft\web\twig\variables\SystemSettings $systemSettings
 * @property \craft\web\twig\variables\UserSession $session
 * @mixin \craft\commerce\web\twig\CraftVariableBehavior
 */
class CraftAutocompleteVariable extends \craft\web\twig\variables\CraftVariable
{
}

/**
 */
class CraftAutocompleteTwigExtension extends \Twig\Extension\AbstractExtension implements \Twig\Extension\GlobalsInterface
{
	public function getGlobals(): array
	{
		return [
			// Craft Variable
			'craft' => new CraftAutocompleteVariable(),
			// Craft Elements
			'asset' => new \craft\elements\Asset(),
			'category' => new \craft\elements\Category(),
			'entry' => new \craft\elements\Entry(),
			'tag' => new \craft\elements\Tag(),
			// Craft "Constants"
			'SORT_ASC' => 4,
			'SORT_DESC' => 3,
			'SORT_REGULAR' => 0,
			'SORT_NUMERIC' => 1,
			'SORT_STRING' => 2,
			'SORT_LOCALE_STRING' => 5,
			'SORT_NATURAL' => 6,
			'SORT_FLAG_CASE' => 8,
			'POS_HEAD' => 1,
			'POS_BEGIN' => 2,
			'POS_END' => 3,
			'POS_READY' => 4,
			'POS_LOAD' => 5,
			// Misc. Craft globals
			'currentUser' => new \craft\elements\User(),
			'currentSite' => new \craft\models\Site(),
			'now' => new \DateTime(),
			// Commerce Elements
			'lineItem' => new \craft\commerce\models\LineItem(),
			'order' => new \craft\commerce\elements\Order(),
			'product' => new \craft\commerce\elements\Product(),
			// Third party globals
			'committeeTools' => new \hlsr\committeetools\CommitteeTools(),
		];
	}
}
