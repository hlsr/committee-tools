<?php
namespace hlsr\committeetools\rosters;

use Craft;
use craft\helpers\StringHelper;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\spreadsheets\SpreadsheetsHelper;
use yii\base\Component;
use yii\base\InvalidArgumentException;

class Rosters extends Component
{

	const FullRoster = 'FullRoster';
	const LightRoster = 'LightRoster';
	const EmergencyContact = 'EmergencyContact';
	const CommitteeServiceHistory = 'CommitteeServiceHistory';

	public $validImportTypes = [self::FullRoster, self::LightRoster, self::EmergencyContact];

	public function sniffImportTypeFromFilename(string $filename): ?string
	{

		switch (true)
		{

			case StringHelper::contains($filename, "Full Committee Roster"):
			case StringHelper::contains($filename, "FullCommitteeRoster"):
				return self::FullRoster;

			case StringHelper::contains($filename, "Light Committee Roster"):
			case StringHelper::contains($filename, "LightCommitteeRoster"):
				return self::LightRoster;

			case StringHelper::contains($filename, "Emergency Contact"):
			case StringHelper::contains($filename, "EmergencyContact"):
				return self::EmergencyContact;

			case StringHelper::contains($filename, "Committee Service History"):
			case StringHelper::contains($filename, "CommitteeServiceHistory"):
				return self::CommitteeServiceHistory;

		}

		return null;

	}

	/**
	 * @throws InvalidArgumentException
	 * @throws \Exception
	 */
	public function importFromFile(string $file, string $importType = null): void
	{

		// Make sure we know how to import this file.
		if (empty($importType))
		{
			$importType = $this->sniffImportTypeFromFilename($file);
		}
		if (!in_array($importType, $this->validImportTypes, true))
		{
			throw new InvalidArgumentException("Invalid import type specified.");
		}

		// Make sure the file path exists
		if (!file_exists($file))
		{
			throw new InvalidArgumentException("The specified file does not exist: {$file}");
		}

		$data = SpreadsheetsHelper::getRowsFromFile($file, true);

		switch($importType)
		{
			case self::FullRoster :
				$this->importFullRosterData($data);
				CommitteeTools::getInstance()->members->updateMemberUsers();
				return;

			case self::LightRoster :
				$this->importLightRosterData($data);
				CommitteeTools::getInstance()->members->updateMemberUsers();
				return;

			case self::EmergencyContact :
				$this->importEmergencyContactData($data);
				return;

		}

		throw new InvalidArgumentException("Can't infer roster type from filename.");

	}

	/**
	 * @throws \yii\db\Exception
	 */
	public function importFullRosterData(array $data): void
	{

		/*
		 * Set all Active members as Inactive for a moment. We'll re-activate them as we re-import them.
		 *
		 * TODO: Make this optional via param, in case the spreadsheet only represents a partial Roster.
		 */
		Craft::$app->db->createCommand()->update(
			MemberRecord::tableName(),
			['status' => MemberRecord::STATUS_INACTIVE],
			['status' => MemberRecord::STATUS_ACTIVE]
		)->execute();

		$success = true;

		foreach ($data as $row)
		{

			try
			{

				// TODO: Change attr names to match new HLSR fields
				$attributes = [
					'cn' => (int) $row['Customer Number'],
					'description' => $row['Title'],
					'customerName' => $row['Name'],
					'customerNamePhonetic' => $row['Full Name'],
					'firstName' => $row['First Name'],
					'lastName' => $row['Last Name'],
					'preferredName' => $row['Preferred Name'],
					'legalNameVerified' => ($row['Legal Name Verified'] ?? '') == 'Y',
					'subcommitteeName1' => $row['Subcommittee 1'] ?? '',
					'subcommitteeName2' => $row['Subcommittee 2'] ?? '',
					'subcommitteeName3' => $row['Subcommittee 3'] ?? '',
					'address' => $row['Address'],
					'city' => $row['City'],
					'state' => $row['State'],
					'zip' => $row['Zip'],
					'phone' => $row['Primary Phone'],
					// 'primaryPhoneType' => $row['Primary Phone Type'],
					'email' => $row['Primary Email'],
					'showDues' => ($row['Show Dues'] ?? '') == 'Y',
					'commDues' => ($row['Committee Dues'] ?? '') == 'Y',
					'indemnityInd' => ($row['Indemnity'] ?? '') == 'Y',
					'harassmentInd' => ($row['Sexual Harassment Training'] ?? '') == 'Y',
					'badgeReleased' => ($row['Badge Released'] ?? '') == 'Y',
					'badgeReleaseDate' => null, // TODO: Fix dirty Excel dates
					'rookie' => ($row['Rookie'] ?? '') == 'Y',
					'badgeIssueDate' => null, // TODO: Fix dirty Excel dates
					'badgePickupPerson' => $row['Badge Pickup Person'] ?? '',
					'ltcApplied' => ($row['LTC Applied'] ?? '') == 'Y',
					'inOtherCommittees' => ($row['In Other Committees'] ?? '') == 'Y',
					'status' => MemberRecord::STATUS_ACTIVE,
				];

				$success = (bool) Craft::$app->db->createCommand()->upsert(
					MemberRecord::tableName(),
					$attributes,
					true
				)->execute() && $success;

			}
			catch (\Exception $e)
			{
				CommitteeTools::error($e->getMessage());
				$success = false;
			}

		}

		if (!$success)
		{
			throw new \Exception("Some records were not updated.");
		}

	}

	public function importLightRosterData(array $data): void
	{

		$errorOccurred = false;

		foreach ($data as $row)
		{

			try
			{

				/*
				 * The Light Roster docs don't have a CUSTOMER_NUMBER column,
				 * but we can be clever and extract it from the PHOTO_URL... for now...
				 */
				$photoUrl = $row['Photo URL'] ?? '';
				$cn = RostersHelper::extractCustomerNumberFromPhotoUrl($photoUrl);

				if (empty($cn))
				{
					throw new \Exception("Unable to extract a Customer Number from URL: {$photoUrl}");
				}

				// TODO: Change attr names to match new HLSR fields
				$attributes = [
					'cn' => $cn,
					'lightRosterTitle' => $row['Title'],
					'lightRosterCustomerName' => $row['Name'],
					'lightRosterCustomerNamePhonetic' => $row['Full Name'],
					'lightRosterFirstName' => $row['First Name'],
					'lightRosterLastName' => $row['Last Name'],
					'lightRosterCommitteeName' => $row['Committee Name'],
					'subcommitteeName1' => $row['Subcommittee 1'],
					'subcommitteeName2' => $row['Subcommittee 2'],
					'subcommitteeName3' => $row['Subcommittee 3'],
					'lightRosterAddress' => $row['Address'],
					'lightRosterCityStateZip' => $row['CityStateZip'],
					'lightRosterHomePhone' => $row['Primary Phone'],
					'lightRosterEmail' => $row['Primary Email'],
					'lightRosterSpouse' => $row['Spouse'],
					'lightRosterEmployer' => $row['Employer'] ?? '',
					'lightRosterJobTitle' => $row['Job Title'] ?? '',
					'photoUrl' => RostersHelper::getNormalizedPhotoUrl($photoUrl),
					'status' => MemberRecord::STATUS_ACTIVE,
				];

				$success = Craft::$app->db->createCommand()->upsert(
					MemberRecord::tableName(),
					$attributes,
					true
				)->execute();

				if (!$success)
				{
					throw new \Exception("Could not update record: {$cn}");
				}

			}
			catch (\Exception $e)
			{
				CommitteeTools::error($e->getMessage());
				$errorOccurred = true;
			}

		}

		if ($errorOccurred)
		{
			throw new \Exception("Some records were not updated.");
		}

	}

	public function importEmergencyContactData(array $data): void
	{

		$success = true;

		foreach ($data as $row)
		{

			try
			{

				$attributes = [
					'cn' => (int) $row['MEMBER_NUMBER'],
					'emergencyContactName' => $row['EMERGENCY_CONTACT_NAME'],
					'emergencyContactRelationship' => $row['RELATIONSHIP'],
					'emergencyContactTelephone' => $row['TELEPHONE'],
					'emergencyContactEmail' => $row['EMAIL'],
				];

				$success = (bool) Craft::$app->db->createCommand()->upsert(
						MemberRecord::tableName(),
						$attributes,
						true
					)->execute() && $success;

			}
			catch (\Exception $e)
			{
				CommitteeTools::error($e->getMessage());
				$success = false;
			}

		}

		if (!$success)
		{
			throw new \Exception("Some records were not updated.");
		}

	}

}
