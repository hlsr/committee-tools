<?php
namespace hlsr\committeetools\rosters;

use craft\base\Component;
use hlsr\committeetools\CommitteeTools;
use yii\base\InvalidArgumentException;

/**
 * @property $name
 * @property $abbreviation
 * @property $displayName
 * @property $nickname
 */
class Subcommittee extends Component
{

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $abbreviation;

	/**
	 * @var string
	 */
	public $displayName;

	/**
	 * @var string
	 */
	public $nickname;

	/**
	 * @param string|array $config Subcommittee name, or config array
	 */
	public function __construct($config = [])
	{
		if (is_string($config))
		{
			$config = ['name' => $config];
		}
		if (empty($config['name']))
		{
			throw new InvalidArgumentException("Subcommittee must be instantiated with a name.");
		}
		parent::__construct($config);
	}

	/**
	 * @return mixed
	 */
	public function __toString()
	{
		return $this->displayName;
	}

	/**
	 *
	 */
	public function init()
	{
		$this->abbreviation = CommitteeTools::getInstance()->subcommittees->getSubcommitteeAbbreviation($this->name);
		$this->displayName = CommitteeTools::getInstance()->subcommittees->getSubcommitteeDisplayName($this->name);
		$this->nickname = CommitteeTools::getInstance()->subcommittees->getSubcommitteeNickname($this->name);
	}

}
