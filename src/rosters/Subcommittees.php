<?php
namespace hlsr\committeetools\rosters;

use craft\db\Query;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use yii\base\Component;

class Subcommittees extends Component
{

	/**
	 * @var array
	 */
	private $subcommittees = [];

	/**
	 * @param $name
	 *
	 * @return Subcommittee|null
	 */
	public function getSubcommittee($name)
	{

		// Trim and check for empty, just in case
		$name = trim($name);

		if ($name) {
			if (!isset($this->subcommittees[$name]))
			{
				$this->subcommittees[$name] = new Subcommittee($name);
			}
			return $this->subcommittees[$name];
		}

		// Don't allow empty names
		return null;

	}

	/**
	 * @param array $names
	 *
	 * @return Subcommittee[]
	 */
	public function getSubcommitteesFromNames($names)
	{

		// Ensure we're working with an array of names
		if (is_string($names)) {
			$names = explode(',', $names);
		}

		$subcommittees = [];

		foreach ($names as $name) {
			$subcommittees[] = $this->getSubcommittee($name);
		}

		// Remove empties
		return array_filter(
			$subcommittees,
			function($s){ return !empty($s); }
		);

	}

	/**
	 * @param string $subcommittee
	 * @param bool $returnFullNameIfNoMatch
	 *
	 * @return string|null
	 */
	public function getSubcommitteeAbbreviation($subcommittee, $returnFullNameIfNoMatch = true)
	{

		$abbreviations = CommitteeTools::getInstance()->getSettings()->subcommitteeAbbreviations;

		if (isset($abbreviations[$subcommittee]))
		{
			return $abbreviations[$subcommittee];
		}

		return $returnFullNameIfNoMatch ? $subcommittee : null;

	}

	/**
	 * @param string $subcommittee
	 * @param bool $returnFullNameIfNoMatch
	 *
	 * @return string|null
	 */
	public function getSubcommitteeDisplayName($subcommittee, $returnFullNameIfNoMatch = true)
	{

		$displayNames = CommitteeTools::getInstance()->getSettings()->subcommitteeDisplayNames;

		if (isset($displayNames[$subcommittee]))
		{
			return $displayNames[$subcommittee];
		}

		return $returnFullNameIfNoMatch ? $subcommittee : null;

	}

	/**
	 * @param string $subcommittee
	 * @param bool $returnFullNameIfNoMatch
	 *
	 * @return string|null
	 */
	public function getSubcommitteeNickname($subcommittee, $returnFullNameIfNoMatch = true)
	{

		$nicknames = CommitteeTools::getInstance()->getSettings()->subcommitteeNicknames;

		if (isset($nicknames[$subcommittee]))
		{
			return $nicknames[$subcommittee];
		}

		return $returnFullNameIfNoMatch ? $subcommittee : null;

	}

	/**
	 * @return Subcommittee[]
	 */
	public function getAllSubcommittees()
	{

		$names = $this->getUniqueSubcommitteeNames();
		natcasesort($names);

		return $this->getSubcommitteesFromNames($names);

	}

	/**
	 * @return array
	 */
	public function getUniqueSubcommitteeNames()
	{

		$query = (new Query())->from(MemberRecord::tableName())->select('subcommitteeName1 as name')->andWhere(['status' => MemberRecord::STATUS_ACTIVE])
			->union(
				(new Query())->from(MemberRecord::tableName())->select('subcommitteeName2 as name')->andWhere(['status' => MemberRecord::STATUS_ACTIVE])
			)
			->union(
				(new Query())->from(MemberRecord::tableName())->select('subcommitteeName3 as name')->andWhere(['status' => MemberRecord::STATUS_ACTIVE])
			);

		return array_filter($query->column());

	}

}
