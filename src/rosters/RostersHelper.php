<?php
namespace hlsr\committeetools\rosters;

class RostersHelper
{

	/**
	 *
	 */
	public static function getNormalizedPhotoUrl(string $str): ?string
	{

		/*
		 * Ordinarily we'd just worry about trimming gunk off the beginning/end of a given URL.
		 * However, at the moment, Light Rosters coming from HLSR are broken, i.e. missing a necessary slash,
		 * so all their URLs are 404'ing.
		 * Brute force it is...
		 */
		$cn = static::extractCustomerNumberFromPhotoUrl($str);
		return "https://secure.rodeohouston.com/Membership/ChairtoolsRoster/RosterPhoto/" . $cn;

		/*
		 * (Should bring this stuff back later, once HLSR fixes the URL issue.)
		 */
		$matches = [];
		// TODO: This regex should probably be more robust.
		preg_match('/http[^\"]+/', $str, $matches);
		return isset($matches[0]) ? $matches[0] : null;

	}

	/**
	 * @param string $url
	 *
	 * @return int|null
	 */
	public static function extractCustomerNumberFromPhotoUrl($url)
	{
		$matches = [];
		preg_match('/RosterPhoto\/?(\d+)/', $url, $matches);
		return isset($matches[1]) ? (int)$matches[1] : null;
	}

	/**
	 * @param $description
	 *
	 * @return int
	 */
	public static function getDescriptionSortValue($description)
	{

		$sortValues = [
			'Chairman' => 77,
			'Officer in Charge' => 76,
			'Division Chairman' => 66,
			'Ambassador' => 65,
			'Vice Chairman' => 55,
			'Captain' => 44,
			'Assistant Captain' => 33,
			'Coordinator' => 22,
			'Past Committee Chairman' => 21
		];

		return $sortValues[$description] ?? 0;

	}

}
