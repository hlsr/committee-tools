<?php
namespace hlsr\committeetools\base;

use craft\db\ActiveRecord;
use craft\helpers\Db;

/**
 * We implement our own TrashableTrait, instead of using Craft's SoftDeleteTrait, for performance gains.
 * (Something in Craft's trait/behavior combo hogs memory and slows any batch operations to a crawl.)
 *
 * This should be implemented by Active Record classes that wish to support soft deletes.
 *
 * The database table should be created with a `dateDeleted` column (type `datetime null`).
 *
 * ```php
 * 'dateDeleted' => $this->dateTime()->null()
 * ```
 *
 * Active Record classes may implement a custom `find()` method
 * that excludes soft-deleted rows by default.
 *
 * ```php
 * public static function find()
 * {
 *     $query = Craft::createObject(MyActiveQuery::class, [static::class]);
 *     $query->where(['dateDeleted' => null]);
 *     return $query;
 * }
 * ```
 *
 * @property ActiveRecord $this
 * @property string|null $dateDeleted Date deleted
 */
trait TrashableTrait
{

	public function trash(): bool
	{
		$this->dateDeleted = Db::prepareDateForDb(new \DateTime());
		return $this->save();
	}

	public function untrash(): bool
	{
		$this->dateDeleted = null;
		return $this->save();
	}

}
