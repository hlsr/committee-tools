<?php
namespace hlsr\committeetools\search;

use Algolia\AlgoliaSearch\SearchClient;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\members\MembersHelper;
use hlsr\committeetools\rosters\RostersHelper;
use yii\base\Component;

class Search extends Component
{

	/**
	 * @param $handle
	 *
	 * @return \Algolia\AlgoliaSearch\SearchIndex
	 */
	public function getIndex($handle)
	{

		$client = SearchClient::create(
			CommitteeTools::getInstance()->getSettings()->algoliaApplicationId,
			CommitteeTools::getInstance()->getSettings()->algoliaAdminApiKey
		);

		return $client->initIndex($handle);

	}

	/**
	 * @return bool
	 */
	public function indexMembers()
	{

		try
		{

			$index = $this->getIndex(CommitteeTools::getInstance()->getSettings()->membersSearchIndexName);
			$index->clearObjects();

			foreach (MemberRecord::find()->batch(400) as $members)
			{

				$memberObjects = array_map(
					function(MemberRecord $member)
					{
						$basicData = [
							'objectID' => $member->cn,
							'fullPreferredName' => MembersHelper::getFullPreferredName($member),
							'status' => ucfirst($member->status),
						];
						$activeMemberData = [
							'subcommittee1DisplayName' => $member->getSubcommittee1() ? $member->getSubcommittee1()->displayName : null,
							'subcommittee2DisplayName' => $member->getSubcommittee2() ? $member->getSubcommittee2()->displayName : null,
							'subcommittee3DisplayName' => $member->getSubcommittee3() ? $member->getSubcommittee3()->displayName : null,
							'descriptionSortValue' => RostersHelper::getDescriptionSortValue($member->description),
							'bestPhoneNumber' => $member->getBestPhoneNumber(),
						];
						if ($member->status === MemberRecord::STATUS_ACTIVE)
						{
							return $basicData + $activeMemberData + $member->getAttributes();
						}
						// TODO: Index Inactive members, so we can access their Committee Service History?
						return null;
					},
					$members
				);

				$index->saveObjects(array_filter($memberObjects));

			}

		}
		catch(\Exception $e)
		{
			CommitteeTools::error($e->getMessage());
			return false;
		}

		return true;

	}

}
