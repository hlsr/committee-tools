<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\CommitteeTools;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Member-related functions...
 *
 * @package hlsr\committeetools\console\controllers
 */
class MembersController extends BaseConsoleController
{


	/*
	 * Public properties
	 */


	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */


	/**
	 * Lists Members without a linked User account.
	 *
	 * @return int
	 */
	public function actionListNonUserMembers()
	{
		// TODO
		return ExitCode::UNAVAILABLE;
	}

	/**
	 * Ensures that all Member records are linked to a User element.
	 *
	 * @return int
	 */
	public function actionEnsureAllActiveMembersAreUsers()
	{

		try
		{
			$success = CommitteeTools::getInstance()->members->ensureAllActiveMembersAreUsers();
		}
		catch (\Throwable $e)
		{
			$this->_writeErr($e->getMessage());
			return ExitCode::UNSPECIFIED_ERROR;
		}

		if ($success)
		{
			$this->stdout('All Members are Users!' . PHP_EOL, Console::FG_GREEN);
			return ExitCode::OK;
		}

		$this->_writeErr('Something went wrong.');
		return ExitCode::UNSPECIFIED_ERROR;

	}

	/**
	 * Updates the User accounts linked to active Members, to match the Member attributes
	 *
	 * @return int
	 */
	public function actionUpdateCurrentMemberUsers()
	{
		return $this->runAndExit(function() {
			CommitteeTools::getInstance()->members->updateMemberUsers();
			$this->stdout('All current Member Users are updated!' . PHP_EOL, Console::FG_GREEN);
		});
	}

	/**
	 * Updates the Members search index in Algolia
	 *
	 * @return int
	 */
	public function actionIndexMembers()
	{

		try
		{
			$success = CommitteeTools::getInstance()->search->indexMembers();
		}
		catch (\Throwable $e)
		{
			$this->_writeErr($e->getMessage());
			return ExitCode::UNSPECIFIED_ERROR;
		}

		if ($success)
		{
			$this->stdout('Updated Members search index.' . PHP_EOL, Console::FG_GREEN);
			return ExitCode::OK;
		}

		$this->_writeErr('Something went wrong.');
		return ExitCode::UNSPECIFIED_ERROR;

	}

	/**
	 * Fetches and downloads the latest Member Photo for each member.
	 *
	 * @return int
	 */
	public function actionUpdateMemberPhotos($cn = null)
	{

		return $this->runAndExit(function() use ($cn) {
			CommitteeTools::getInstance()->members->updateMemberPhotos($cn);
			$this->stdout('Updated member photos.' . PHP_EOL, Console::FG_GREEN);
		});

	}


}
