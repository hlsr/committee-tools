<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\checkins\CheckinRecord;
use hlsr\committeetools\CommitteeTools;
use yii\console\ExitCode;

/**
 * Check-in functions...
 *
 * @package hlsr\committeetools\console\controllers
 */
class CheckinsController extends BaseConsoleController
{

	/*
	 * Public properties
	 */

	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';

	/**
	 * @throws \Exception
	 */
	public function actionReplayAllCheckins(): int
	{

		$checkins = CheckinRecord::find()->all();
		$success = true;

		foreach ($checkins as $checkin)
		{
			$success = CommitteeTools::getInstance()->checkins->replayCheckin($checkin) && $success;
		}

		return ExitCode::OK;

	}

	/*
	 * Public methods
	 */

}
