<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\CommitteeTools;
use yii\console\ExitCode;

/**
 * Assignment-Request-related functions...
 *
 * @package hlsr\committeetools\console\controllers
 */
class ShiftAssignmentRequestsController extends BaseConsoleController
{

	/*
	 * Public properties
	 */

	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */

	/**
	 * Voids all Pending Requests.
	 *
	 * @return int
	 */
	public function actionVoidAllPendingRequests()
	{
		// TODO
		return ExitCode::UNAVAILABLE;
	}

	/**
	 * Voids all Pending Requests.
	 *
	 * @return int
	 */
	public function actionProcessExpiredRequests()
	{

		// TODO: Notification to Rollbar

		$success = CommitteeTools::getInstance()->assignmentRequests->processExpiredRequests();

		if (!$success)
		{
			$this->_writeErr("Something went wrong when processing the expired requests.");
			return ExitCode::UNSPECIFIED_ERROR;
		}

		return ExitCode::OK;

	}

	public function actionCancelRequest(int $requestId): int
	{
		$request = CommitteeTools::getInstance()->assignmentRequests->getRequestById($requestId);
		$success = $request && CommitteeTools::getInstance()->assignmentRequests->cancelRequest($request);
		if (!$success)
		{
			$this->_writeErr("That request could not be found, or could not be cancelled.");
			return ExitCode::UNSPECIFIED_ERROR;
		}
		return ExitCode::OK;
	}

	public function actionVoidRequest(int $requestId): int
	{
		$request = CommitteeTools::getInstance()->assignmentRequests->getRequestById($requestId);
		$success = $request && CommitteeTools::getInstance()->assignmentRequests->voidRequest($request);
		if (!$success)
		{
			$this->_writeErr("That request could not be found, or could not be voided.");
			return ExitCode::UNSPECIFIED_ERROR;
		}
		return ExitCode::OK;
	}



}
