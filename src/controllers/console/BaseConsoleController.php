<?php
namespace hlsr\committeetools\controllers\console;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

abstract class BaseConsoleController extends Controller
{

	/**
	 * Writes an error to the console
	 */
	protected function _writeErr($msg)
	{
		$this->stderr('Error: ', Console::BOLD, Console::FG_RED);
		$this->stderr(print_r($msg, true) . PHP_EOL);
	}

	/**
	 * Writes a line to the console
	 */
	protected function _writeLine($msg)
	{
		$this->stdout(print_r($msg, true) . PHP_EOL);
	}

	/**
	 * Runs the given function and returns an appropriate error code, writing out errors as needed.
	 */
	protected function runAndExit(callable $function, $profile = false): int
	{

		$startTime = microtime(true);

		try
		{
			$function();
			$profile && $this->_writeLine("(Completed in " . (microtime(true) - $startTime) . "s.)");
			return ExitCode::OK;
		}
		catch (\Exception $e)
		{
			$profile && $this->_writeLine("(Failed after " . (microtime(true) - $startTime) . "s.)");
			$this->_writeErr($e->getMessage());
			return ExitCode::UNSPECIFIED_ERROR;
		}

	}

}
