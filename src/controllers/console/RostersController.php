<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\CommitteeTools;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Roster-related functions...
 *
 * @package hlsr\committeetools\console\controllers
 */
class RostersController extends BaseConsoleController
{


	/*
	 * Public properties
	 */


	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */


	/**
	 * Imports Member data from a spreadsheet file.
	 */
	public function actionImport(string $file, string $recordDate = null): int
	{

		// Make sure our parameters have been defined...

		if (empty($file))
		{
			$this->_writeErr('--file=/path/to/spreadsheet/file is required.');
			return ExitCode::USAGE;
		}

		return $this->runAndExit(function() use ($file) {
			CommitteeTools::getInstance()->rosters->importFromFile($file);
			$this->stdout("Import complete!" . PHP_EOL, Console::FG_GREEN);
		});

	}


}
