<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\CommitteeTools;

/**
 * Wishlist-related functions...
 */
class ShiftWishlistsController extends BaseConsoleController
{

	/*
	 * Public properties
	 */

	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */

	/**
	 * Create a wishlist entry for a given member and shift.
	 */
	public function actionAddShiftToMemberWishlist(int $cn, int $shiftId): int
	{
		return $this->runAndExit(function() use ($cn, $shiftId) {
			CommitteeTools::getInstance()->wishlists->addShiftToMemberWishlist($cn, $shiftId);
		});
	}

	/**
	 * Remove all wishlist entries for a given member.
	 */
	public function actionClearMemberWishlist(int $cn): int
	{
		return $this->runAndExit(function() use ($cn) {
			CommitteeTools::getInstance()->wishlists->clearMemberWishlist($cn);
		});
	}

	/**
	 * Remove wishlist entry for a given member and shift.
	 */
	public function actionRemoveShiftFromMemberWishlist(int $cn, int $shiftId): int
	{
		return $this->runAndExit(function() use ($cn, $shiftId) {
			CommitteeTools::getInstance()->wishlists->removeShiftFromMemberWishlist($cn, $shiftId);
		});
	}

}
