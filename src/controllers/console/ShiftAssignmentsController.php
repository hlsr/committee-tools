<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;
use yii\console\ExitCode;

/**
 * Assignment-related functions...
 *
 * @package hlsr\committeetools\console\controllers
 */
class ShiftAssignmentsController extends BaseConsoleController
{

	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';

	/**
	 * Creates Member assignment for a Shift
	 */
	public function actionCreateMemberAssignmentForShift(int $cn, int $shiftId): int
	{

		return $this->runAndExit(function() use ($cn, $shiftId) {

			$member = CommitteeTools::getInstance()->members->getMemberByCn($cn);
			if (!$member)
			{
				throw new \Exception("Member not found.");
			}

			$shift = CommitteeTools::getInstance()->shifts->getShiftById($shiftId);
			if (!$shift)
			{
				throw new \Exception("Shift not found.");
			}

			CommitteeTools::getInstance()->assignments->createMemberAssignmentForShift(
				$member,
				$shift,
				['type' => ShiftAssignmentRecord::TYPE_INDIVIDUAL],
				true,
			);

		});

	}

	/**
	 * Creates subcommittee Assignments for the Shifts in the system.
	 */
	public function actionCreateSubcommitteeAssignments(): int
	{
		return $this->runAndExit(function() {
			CommitteeTools::getInstance()->assignments->createSubcommitteeAssignmentsForAllShifts();
		}, true);
	}

	/**
	 * Creates unassigned assignments for a Shift
	 */
	public function actionCreatePickupAssignmentsForShift(int $shiftId, int $qty): int
	{

		return $this->runAndExit(function() use ($shiftId, $qty) {

			$shift = CommitteeTools::getInstance()->shifts->getShiftById($shiftId);

			if (!$shift)
			{
				throw new \Exception("Shift not found.");
			}

			if ($this->confirm("Create {$qty} unassigned shift assignments for Shift {$shiftId}?"))
			{
				CommitteeTools::getInstance()->assignments->createUnassignedAssignmentsForShift(
					$shift,
					$qty,
					[
						'type' => ShiftAssignmentRecord::TYPE_INDIVIDUAL,
						'availableForPickup' => true,
					],
				);
			}

		});

	}

	/**
	 * Delete a Shift Assignment
	 */
	public function actionDeleteAssignment(int $assignmentId): int
	{

		return $this->runAndExit(function() use ($assignmentId) {

			$assignment = CommitteeTools::getInstance()->assignments->getAssignmentById($assignmentId);

			if (!$assignment)
			{
				throw new \Exception("Shift assignment not found.");
			}

			if (!$assignment->trash())
			{
				throw new \Exception("Error moving assignment to trash.");
			};

		});

	}

	/**
	 * Imports Shift Assignment adjustments from a spreadsheet file.
	 */
	public function actionImport(string $file): int
	{

		return $this->runAndExit(function() use ($file) {
			CommitteeTools::getInstance()->assignments->importFromFile($file);
		}, true);

	}

	/**
	 * Refresh a Shift Assignment's Pending Requests flags
	 */
	public function actionRefreshPendingRequestsStatus(int $assignmentId): int
	{

		$assignments = CommitteeTools::getInstance()->assignments;

		$assignment = $assignments->getAssignmentById($assignmentId);

		if (!$assignment)
		{
			$this->_writeErr("Shift Assignment not found.");
			return ExitCode::UNSPECIFIED_ERROR;
		}

		$success = $assignments->refreshPendingRequestsStatus($assignment);

		return $success ? ExitCode::OK : ExitCode::UNSPECIFIED_ERROR;

	}

}
