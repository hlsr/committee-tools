<?php
namespace hlsr\committeetools\controllers\console;

use Craft;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\shifts\ShiftElement;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Shift-related functions...
 *
 * @package hlsr\committeetools\console\controllers
 */
class ShiftsController extends BaseConsoleController
{


	/*
	 * Public properties
	 */


	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */


	/**
	 * Imports Shifts from a spreadsheet file.
	 *
	 * @throws \Throwable
	 */
	public function actionImport(string $file = null): int
	{

		// Make sure our parameters have been defined...

		if (empty($file))
		{
			$this->_writeErr('--file=/path/to/spreadsheet/file is required.');
			return ExitCode::USAGE;
		}

		try
		{
			$success = CommitteeTools::getInstance()->shifts->importFromFile($file);
		}
		catch (\Exception $e)
		{
			$this->_writeErr($e->getMessage());
			return ExitCode::UNSPECIFIED_ERROR;
		}

		if ($success)
		{
			$this->stdout('Import complete!' . PHP_EOL, Console::FG_GREEN);
			return ExitCode::OK;
		}

		$this->_writeErr('Something went wrong during the import.');
		return ExitCode::UNSPECIFIED_ERROR;

	}

	/**
	 * Delete a Shift by ID.
	 */
	public function actionDeleteShift(int $shiftId): int
	{
		return $this->runAndExit(function() use ($shiftId) {

			$shift = ShiftElement::findOne($shiftId);
			if (!$shift)
			{
				throw new \Exception("Can't find a Shift with that ID.");
			}
			if (!Craft::$app->elements->deleteElement($shift, true))
			{
				throw new \Exception("Couldn't delete this Shift.");
			}

		});
	}

}
