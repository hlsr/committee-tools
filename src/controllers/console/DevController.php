<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\rosters\RostersHelper;
use yii\console\ExitCode;

/**
 * Testing functions, etc...
 *
 * @package hlsr\committeetools\console\controllers
 */
class DevController extends BaseConsoleController
{


	/*
	 * Public properties
	 */


	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */

	public function actionExtractCnFromPhotoUrl($url)
	{

		echo RostersHelper::extractCustomerNumberFromPhotoUrl($url) . PHP_EOL;

		return ExitCode::OK;

	}

	public function actionTestEmailNotification()
	{

		// TODO: Send this to system email.
		$success = CommitteeTools::getInstance()->notifications->sendEmailNotification(
			"Test Email Notification",
			"michael@michaelrog.com",
			'_notifications/test'
		);

		return $success ? ExitCode::OK : ExitCode::UNSPECIFIED_ERROR;

	}

	public function actionSaveMemberImage($cn)
	{

		try
		{
			CommitteeTools::getInstance()->members->saveMemberPhoto($cn);
			return 1;
		}
		catch(\Throwable $e)
		{
			$this->_writeErr($e->getMessage());
			return 0;
		}

	}


}
