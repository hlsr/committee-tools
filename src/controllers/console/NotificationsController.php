<?php
namespace hlsr\committeetools\controllers\console;

use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use yii\console\ExitCode;

/**
 * Notifications functions...
 */
class NotificationsController extends BaseConsoleController
{

	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';

	public function actionSendSmsToSubcommittee(string $subcommittee, string $message): int
	{

		$notifications = CommitteeTools::getInstance()->notifications;

		$phoneNumbers = array_map(function(MemberRecord $member) use ($notifications){
			return $member->getBestPhoneNumber();
		}, CommitteeTools::getInstance()->members->getMembersBySubcommittee($subcommittee));

		try
		{
			$success = $notifications->sendSmsMessage($message, $phoneNumbers);
			return $success ? ExitCode::OK : ExitCode::UNSPECIFIED_ERROR;
		}
		catch(\Exception $e)
		{
			$this->_writeErr($e->getMessage());
		}

		return ExitCode::UNSPECIFIED_ERROR;

	}

	public function actionSendSmsToShiftAssignees(int $shiftId, string $message, bool $checkedInOnly = false): int
	{

		return $this->runAndExit(function() use ($shiftId, $message, $checkedInOnly) {
			CommitteeTools::getInstance()->notifications->sendSmsToShiftAssignees($shiftId, $message, $checkedInOnly);
		});

	}

}
