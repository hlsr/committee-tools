<?php
namespace hlsr\committeetools\controllers\web;

use craft\helpers\FileHelper;
use craft\web\UploadedFile;
use hlsr\committeetools\CommitteeTools;
use yii\web\Response;

/**
 * @package hlsr\committeetools\controllers\web
 */
class RostersController extends BaseWebController
{

	/*
	 * Public properties
	 */

	/**
	 * @var bool
	 */
	public $allowAnonymous = false;

	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */

	/**
	 * Imports Shifts from a spreadsheet file.
	 */
	public function actionImport(): Response
	{

		$this->requirePostRequest();

		// Make sure our parameters have been defined...

		$uploadedFile = UploadedFile::getInstanceByName('file', true);

		if (empty($uploadedFile))
		{
			return $this->returnErrorResponse("No file given.");
		}

		$importType = CommitteeTools::getInstance()->rosters->sniffImportTypeFromFilename($uploadedFile->name);

		if (empty($importType))
		{
			return $this->returnErrorResponse("Could not determine the import type from this file.");
		}

		try
		{

			// Process the import

			CommitteeTools::getInstance()->rosters->importFromFile($uploadedFile->tempName, $importType);

			// Try to save the file

			$savePath = CommitteeTools::getInstance()->getSettings()->rostersUploadPath;
			if ($savePath)
			{
				FileHelper::createDirectory($savePath);
				$uploadedFile->saveAs($savePath.'/'.$uploadedFile->name);
			}

			// Kick back a return

			return $this->redirectToPostedUrl();

		}
		catch (\Exception $e)
		{
			return $this->returnErrorResponse($e->getMessage());
		}

	}

}
