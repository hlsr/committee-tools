<?php
namespace hlsr\committeetools\controllers\web;

use Craft;
use craft\web\UploadedFile;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;
use hlsr\committeetools\shifts\ShiftAssignments;
use hlsr\committeetools\shifts\ShiftAssignmentStatusUpdateRecord;
use yii\base\Exception;
use yii\web\Response;

class ShiftAssignmentsController extends BaseWebController
{

	public $allowAnonymous = false;

	/**
	 * Creates Member assignment for a Shift
	 */
	public function actionCreateMemberAssignmentForShift(): ?Response
	{

		$request = Craft::$app->getRequest();

		$member = CommitteeTools::getInstance()->members->getMemberByCn($request->getRequiredParam('cn'));

		if (!$member)
		{
			return $this->returnErrorResponse("Member could not be found.");
		}

		$shift = CommitteeTools::getInstance()->shifts->getShiftById($request->getRequiredParam('shift'));

		if (!$shift)
		{
			return $this->returnErrorResponse("Shift not found.");
		}

		try
		{

			CommitteeTools::getInstance()->assignments->createMemberAssignmentForShift(
				$member,
				$shift,
				['type' => ShiftAssignmentRecord::TYPE_INDIVIDUAL],
				true
			);

			return $this->returnSuccessResponse();

		}
		catch (\Exception $e)
		{
			CommitteeTools::error($e->getMessage());
		}

		return $this->returnErrorResponse("The Shift Assignment could not be created.");

	}

	/**
	 * Creates unassigned assignments for a Shift
	 */
	public function actionCreatePickupAssignmentsForShift(): ?Response
	{

		return $this->runAndReturn(function() {

			$request = Craft::$app->getRequest();

			$shift = CommitteeTools::getInstance()->shifts->getShiftById($request->getRequiredParam('shift'));

			if (!$shift)
			{
				throw new \Exception("Shift not found.");
			}

			$qty = (int) $request->getRequiredParam('qty');

			CommitteeTools::getInstance()->assignments->createUnassignedAssignmentsForShift(
				$shift,
				$qty,
				[
					'type' => ShiftAssignmentRecord::TYPE_INDIVIDUAL,
					'availableForPickup' => true,
				],
			);

		});

	}

	public function actionPickUpAssignment(): ?Response
	{

		return $this->runAndReturn(function() {

			$ct = CommitteeTools::getInstance();
			$request = Craft::$app->getRequest();

			$memberCn = $request->getParam('member');
			$member = $memberCn
				? $ct->members->getMemberByCn($memberCn)
				: $ct->getCurrentMember();

			if (!$member)
			{
				throw new \Exception("Invalid member.");
			}

			$assignmentId = $request->getRequiredParam('assignment');
			$assignment = $ct->assignments->getAssignmentById($assignmentId);

			if (!$assignment)
			{
				throw new \Exception("Invalid shift assignment.");
			}

			if (!$member->canPickUpShiftAssignment($assignment))
			{
				throw new \Exception("Current member is not allowed to pick up this shift assignment.");
			}

			$assignment->assignedMemberCn = $member->cn;
			$assignment->availableForPickup = false;
			$assignment->pickupCompleted = true;

			if (!$assignment->save())
			{
				throw new \Exception("Could not update the Assignment.");
			}

		});

	}

	/**
	 * @return Response
	 */
	public function actionDeleteAssignment(): ?Response
	{

		// TODO: Verify permissions

		return $this->runAndReturn(function() {

			$request = Craft::$app->getRequest();

			$assignmentId = $request->getRequiredParam('assignment');
			$assignment = CommitteeTools::getInstance()->assignments->getAssignmentById($assignmentId);

			if (!$assignment)
			{
				throw new \Exception("Shift assignment not found.");
			}

			// TODO: Void any outstanding requests involving this Assignment

			if (!$assignment->trash())
			{
				throw new \Exception("Error moving assignment to trash.");
			}

		});

	}

	/**
	 * @return Response
	 */
	public function actionSetAssignmentAvailableForTrade(): ?Response
	{

		try
		{

			// TODO: Rename params

			$request = Craft::$app->getRequest();

			$id = $request->getRequiredParam('id');
			$available = $request->getParam('available', true);
			$available = filter_var($available, FILTER_VALIDATE_BOOLEAN);

			$assignment = CommitteeTools::getInstance()->assignments->getAssignmentById($id);

			$assignment->availableForTrade = $available;
			$assignment->save();

			// TODO: When marking unavailable, deny any Trade requests pending for this Assigment.

			return $this->returnSuccessResponse();

		}
		catch(\Exception $e)
		{
			$this->returnErrorResponse($e->getMessage());
		}

	}

	/**
	 * @return Response
	 */
	public function actionUpdateAssignment(): ?Response
	{

		$request = Craft::$app->getRequest();

		// TODO: Verify permissions

		try
		{

			// TODO: Db transaction

			$assignmentId = $request->getRequiredParam('assignment');

			$assignment = CommitteeTools::getInstance()->assignments->getAssignmentById($assignmentId);

			if (!$assignment)
			{
				throw new Exception("No Assignment exists with the provided ID.");
			}

			$assignedMemberCn = $request->getParam('assignedMemberCn');
			if (isset($assignedMemberCn))
			{
				$assignment->assignedMemberCn = $assignedMemberCn;
			}

			$area = $request->getParam('area');
			if (isset($area))
			{
				$assignment->area = $area;
			}

			$position = $request->getParam('position');
			if (isset($position))
			{
				$assignment->position = $position;
			}

			$note = $request->getParam('note');
			if (isset($note))
			{
				$assignment->note = $note;
			}

			$status = $request->getParam('status');
			if (isset($status))
			{

				// TODO: Refactor into its own status-setter method

				// If we're changing the status, create a Status Update Record.

				if ($assignment->status !== $status)
				{

					$assignment->status = $status;

					$statusUpdateRecord = new ShiftAssignmentStatusUpdateRecord([
						'shiftAssignmentId' => $assignment->id,
						'status' => $status,
						'updatedByUserId' => Craft::$app->getUser()->getId(),
						'updatedFromLocation' => null, // TODO: Implement updatedFromLocation
					]);
					$statusUpdateRecord->save();

					// If the new status "Checks In" the member...
					if
					(
						empty($assignment->checkedInTime)
						&&
						in_array($status, ShiftAssignments::CHECKED_IN_STATUSES)
					)
					{
						$assignment->checkedInTime = new \DateTime();
						$assignment->checkedInByUserId = Craft::$app->getUser()->getId();
					}

				}

			}

			if (!$assignment->save())
			{
				throw new \Exception("Could not update the Assignment.");
			}

			return $this->returnSuccessResponse();

		}
		catch(\Throwable $e)
		{
			return $this->returnErrorResponse($e->getMessage());
		}

	}

	/**
	 * Imports Shifts from a spreadsheet file.
	 */
	public function actionImport(): ?Response
	{

		$this->requirePostRequest();

		return $this->runAndReturn(function() {

			$uploadedFile = UploadedFile::getInstanceByName('file', true);

			if (empty($uploadedFile))
			{
				throw new \Exception("No file given.");
			}

			CommitteeTools::getInstance()->assignments->importFromFile($uploadedFile->tempName);

		});

	}

}
