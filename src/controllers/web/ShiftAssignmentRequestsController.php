<?php
namespace hlsr\committeetools\controllers\web;

use Craft;
use craft\web\Request;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class ShiftAssignmentRequestsController extends BaseWebController
{

	/**
	 * @var bool
	 */
	public $allowAnonymous = false;

	/**
	 * @var MemberRecord
	 */
	private $_currentMember;

	/**
	 * @var Request
	 */
	private $_request;


	/**
	 * @throws BadRequestHttpException
	 * @throws InvalidConfigException
	 */
	public function init()
	{

		parent::init();

		// All this stuff is locked down.
		$this->requirePostRequest();

		// Set a handy reference to the currently logged in Member User
		$currentUser = Craft::$app->getUser()->getIdentity();
		$this->_currentMember = CommitteeTools::getInstance()->members->getUserMember($currentUser);

		// Require a logged in Member User
		if (empty($this->_currentMember))
		{
			throw new BadRequestHttpException("This action requires a logged in Member User.");
		}

		// Set a handy reference to the Request service.
		$this->_request = Craft::$app->getRequest();

	}

	/**
	 * @return Response|null
	 *
	 * @throws BadRequestHttpException
	 */
	public function actionCreateTradeRequest()
	{

		// TODO: Check that Shift Assignments aren't locked.

		$sourceAssignment = CommitteeTools::getInstance()->assignments->getAssignmentById(
			$this->_request->getParam('sourceAssignment')
		);
		$targetAssignment = CommitteeTools::getInstance()->assignments->getAssignmentById(
			$this->_request->getParam('targetAssignment')
		);

		if (empty($sourceAssignment))
		{
			return $this->returnErrorResponse("The source Assignment could not be found. This request cannot be submitted.");
		}

		if (empty($targetAssignment))
		{
			return $this->returnErrorResponse("The target Assignment could not be found. This request cannot be submitted.");
		}

		try
		{
			// TODO: Check current User Member for permissions.
			$tradeRequest = CommitteeTools::getInstance()->assignmentRequests->createTradeRequest($sourceAssignment, $targetAssignment);
		}
		catch (\Exception $e)
		{
			return $this->returnErrorResponse($e->getMessage());
		}

		if (!$tradeRequest)
		{
			return $this->returnErrorResponse("Sorry, something went wrong. We can't process this Trade Request right now.");
		}

		// TODO: Ajax support
		return $this->redirectToPostedUrl($tradeRequest);

	}

	/**
	 * @return Response|null
	 *
	 * @throws BadRequestHttpException
	 */
	public function actionApproveRequest()
	{

		// TODO: Check that Shift Assignments aren't locked.

		$assignmentRequest = CommitteeTools::getInstance()->assignmentRequests->getRequestById(
			$this->_request->getParam('assignmentRequest')
		);

		// Check permissions

		if (!$this->_currentMember->canResolveShiftAssignmentRequest($assignmentRequest))
		{
			return $this->returnErrorResponse("Sorry, you don't have permission to approve this Trade Request.");
		}

		// Do the business

		try
		{
			// TODO: Check current User Member for permissions.
			$success = CommitteeTools::getInstance()->assignmentRequests->approveRequest($assignmentRequest);
		}
		catch (\Exception $e)
		{
			return $this->returnErrorResponse($e->getMessage());
		}

		if (!$success)
		{
			return $this->returnErrorResponse("Sorry, something went wrong. We can't approve this Trade Request right now.");
		}

		// TODO: Ajax support
		return $this->redirectToPostedUrl($assignmentRequest);

	}

	/**
	 * @return Response|null
	 *
	 * @throws BadRequestHttpException
	 */
	public function actionDeclineRequest()
	{

		$assignmentRequest = CommitteeTools::getInstance()->assignmentRequests->getRequestById(
			$this->_request->getParam('assignmentRequest')
		);

		// Check permissions

		if (!$this->_currentMember->canResolveShiftAssignmentRequest($assignmentRequest))
		{
			return $this->returnErrorResponse("Sorry, you don't have permission to approve this Trade Request.");
		}

		// Do the business

		try
		{
			$success = CommitteeTools::getInstance()->assignmentRequests->declineRequest($assignmentRequest);
		}
		catch (\Exception $e)
		{
			return $this->returnErrorResponse($e->getMessage());
		}

		if (!$success)
		{
			return $this->returnErrorResponse("Sorry, something went wrong. We can't decline this Trade Request right now.");
		}

		// TODO: Ajax support
		return $this->redirectToPostedUrl($assignmentRequest);

	}

}
