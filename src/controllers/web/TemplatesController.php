<?php
namespace hlsr\committeetools\controllers\web;

use craft\controllers\TemplatesController as CraftTemplatesController;
use hlsr\committeetools\CommitteeTools;
use yii\web\Response;

class TemplatesController extends CraftTemplatesController
{

	public function actionRender(string $template, array $variables = []): Response
	{

		$settings = CommitteeTools::getInstance()->getSettings();

		$overrideTemplate = $settings->customTemplatePrefix . $template;
		if ($this->getView()->doesTemplateExist($overrideTemplate))
		{
			return parent::actionRender($overrideTemplate, $variables);
		}

		return parent::actionRender($settings->systemTemplateRoot . '/' .$template, $variables);

	}

}
