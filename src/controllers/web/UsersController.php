<?php
namespace hlsr\committeetools\controllers\web;

use Craft;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use yii\web\Response;

class UsersController extends BaseWebController
{

	public $allowAnonymous = true;

	/**
	 * @return Response
	 */
	public function actionSetInitialPassword()
	{

		$request = Craft::$app->getRequest();

		$cn = $request->getParam('cn');
		$email = $request->getParam('email');

		$requestVars = [
			'cn' => $cn,
			'email' => $email,
		];


		$member = MemberRecord::findOne([
			'cn' => $cn,
			'email' => $email,
		]);

		if (!$member)
		{
			return $this->returnErrorResponse(
				"Sorry, those credentials didn't match. Please double-check your Member Number and email address, and try again.",
				$requestVars
			);
		}

		if (!CommitteeTools::getInstance()->members->ensureMemberIsUser($member))
		{
			return $this->returnErrorResponse(
				"Something went wrong; We're not able to create a User account using your Member info.",
				$requestVars
			);
		}

		$user = CommitteeTools::getInstance()->members->getMemberUser($member);

		// TODO: Only allow this method for brand new Members, who haven't logged in yet?
		if ($user->lastLoginDate !== null)
		{
//			return $this->returnErrorResponse(
//				"This function is only for brand-new Members. You've already logged into your account.",
//				$requestVars
//			);
		}

		// Send them to Password Reset
		$resetUrl = Craft::$app->getUsers()->getPasswordResetUrl($user);
		return $this->redirect($resetUrl);

	}

}
