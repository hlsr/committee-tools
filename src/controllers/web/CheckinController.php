<?php
namespace hlsr\committeetools\controllers\web;

use Craft;
use hlsr\committeetools\CommitteeTools;
use yii\web\Response;

class CheckinController extends BaseWebController
{

	public $allowAnonymous = true;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		$this->enableCsrfValidation = false;
		parent::init();
	}

	/**
	 * @return Response
	 */
	public function actionCheckin(): Response
	{

		$request = Craft::$app->getRequest();

		$location = $request->getParam('location');
		$cn = $request->getParam('cn');

		$success = CommitteeTools::getInstance()->checkins->checkInMember($cn, $location);

		// TODO: This getMemberByCn query is redundant. Improve perf somehow?
		$member = CommitteeTools::getInstance()->members->getMemberByCn($cn);

		if ($success)
		{
			return $this->asJson([
				'success' => true,
				'messageType' => 'success',
				'message' => "Welcome " . ($member->preferredName ?: $member->firstName) . " &mdash; Have a great shift!",
				'messageDuration' => 2000,
			]);
		}

		return null;

	}

}
