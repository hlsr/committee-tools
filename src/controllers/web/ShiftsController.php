<?php
namespace hlsr\committeetools\controllers\web;

use craft\helpers\FileHelper;
use craft\web\UploadedFile;
use hlsr\committeetools\CommitteeTools;

/**
 * @package hlsr\committeetools\controllers\web
 */
class ShiftsController extends BaseWebController
{

	/*
	 * Public properties
	 */

	/**
	 * @var bool
	 */
	public $allowAnonymous = false;

	/**
	 * @var string $defaultAction
	 */
	public $defaultAction = '';


	/*
	 * Public methods
	 */

	/**
	 * Imports Shifts from a spreadsheet file.
	 */
	public function actionImport()
	{

		$this->requirePostRequest();

		// Make sure our parameters have been defined...

		$uploadedFile = UploadedFile::getInstanceByName('file', true);

		if (empty($uploadedFile))
		{
			return $this->returnErrorResponse("No file given.");
		}

		try
		{

			// Process the import

			$success = CommitteeTools::getInstance()->shifts->importFromFile($uploadedFile->tempName);

			// Try to save the file

			$savePath = CommitteeTools::getInstance()->getSettings()->shiftsUploadPath;
			if ($savePath)
			{
				FileHelper::createDirectory($savePath);
				$uploadedFile->saveAs($savePath.'/'.$uploadedFile->name);
			}

			// Kick back a return

			if ($success)
			{
				return $this->redirectToPostedUrl();
			}

			return $this->returnErrorResponse("Something went wrong.");

		}
		catch (\Exception $e)
		{
			return $this->returnErrorResponse($e->getMessage());
		}

	}

}
