<?php
namespace hlsr\committeetools\controllers\web;

use Craft;
use craft\web\Controller;
use hlsr\committeetools\CommitteeTools;
use yii\web\BadRequestHttpException;
use yii\web\Response;

abstract class BaseWebController extends Controller
{

	/**
	 * @param int|null $cn
	 * @throws BadRequestHttpException
	 */
	protected function requireCurrentMember(int $cn = null)
	{

		$currentMember = CommitteeTools::getInstance()->getCurrentMember();

		if (!$currentMember)
		{
			throw new BadRequestHttpException("This action requires a logged in Member User.");
		}

		if ($cn && ($cn != $currentMember->cn))
		{
			throw new BadRequestHttpException("The current Member User is forbidden for this action.");
		}

	}

	protected function returnErrorResponse(string $errorMessage, array $routeParams = []): ?Response
	{

		if (Craft::$app->getRequest()->getAcceptsJson())
		{
			return $this->asErrorJson($errorMessage);
		}

		Craft::$app->getSession()->setError($errorMessage);

		Craft::$app->getUrlManager()->setRouteParams([
				'errorMessage' => $errorMessage,
			] + $routeParams);

		return null;

	}

	/**
	 * @throws BadRequestHttpException from `redirectToPostedUrl()` if the redirect param was tampered with.
	 */
	protected function returnSuccessResponse($returnUrlObject = null): Response
	{

		if (Craft::$app->getRequest()->getAcceptsJson())
		{
			return $this->asJson(['success' => true]);
		}

		return $this->redirectToPostedUrl($returnUrlObject, Craft::$app->getRequest()->getReferrer());

	}

	protected function runAndReturn(callable $function): ?Response
	{
		try
		{
			$returnUrlObject = $function();
			return $this->returnSuccessResponse($returnUrlObject);
		}
		catch (\Exception $e)
		{
			return $this->returnErrorResponse($e->getMessage());
		}
	}

}
