<?php
namespace hlsr\committeetools\controllers\web;

use hlsr\committeetools\CommitteeTools;
use yii\web\Response;

class NotificationsController extends BaseWebController
{

	public $allowAnonymous = false;

	public function actionSendSmsToShiftAssignees(): ?Response
	{

		$this->requirePostRequest();
		// TODO: Permissions!

		return $this->runAndReturn(function() {

			$message = $this->request->getRequiredParam('message');
			$fullShiftIds = $this->request->getParam('fullShifts', []);
			$checkedInShiftIds = $this->request->getParam('checkedInShifts', []);

			// TODO: Better error handling!

			foreach ($fullShiftIds as $id)
			{
				CommitteeTools::getInstance()->notifications->sendSmsToShiftAssignees($id, $message);
			}

			foreach ($checkedInShiftIds as $id)
			{
				CommitteeTools::getInstance()->notifications->sendSmsToShiftAssignees($id, $message, true);
			}

		});

	}

}
