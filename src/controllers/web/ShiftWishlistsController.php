<?php
namespace hlsr\committeetools\controllers\web;

use Craft;
use hlsr\committeetools\CommitteeTools;
use yii\web\Response;

class ShiftWishlistsController extends BaseWebController
{

	public $allowAnonymous = false;

	public function beforeAction($action)
	{

		$request = Craft::$app->getRequest();
		$cn = $request->getRequiredParam('cn');
		$this->requireCurrentMember($cn);

		return parent::beforeAction($action);

	}

	public function actionAddShiftToMemberWishlist(): ?Response
	{

		return $this->runAndReturn(function() {

			$request = Craft::$app->getRequest();
			$cn = $request->getRequiredParam('cn');
			$shiftId = $request->getRequiredParam('shiftId');

			CommitteeTools::getInstance()->wishlists->addShiftToMemberWishlist($cn, $shiftId);

		});

	}

	public function actionClearMemberWishlist(): ?Response
	{

		return $this->runAndReturn(function() {

			$request = Craft::$app->getRequest();
			$cn = $request->getRequiredParam('cn');

			CommitteeTools::getInstance()->wishlists->clearMemberWishlist($cn);

		});

	}

	public function actionRemoveShiftFromMemberWishlist(): ?Response
	{

		return $this->runAndReturn(function() {

			$request = Craft::$app->getRequest();
			$cn = $request->getRequiredParam('cn');
			$shiftId = $request->getRequiredParam('shiftId');

			CommitteeTools::getInstance()->wishlists->removeShiftFromMemberWishlist($cn, $shiftId);

		});

	}

}
