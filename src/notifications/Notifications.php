<?php
namespace hlsr\committeetools\notifications;

use craft\helpers\StringHelper;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;
use hlsr\committeetools\view\TemplateHelper;
use Twilio\Rest\Client;
use yii\base\Component;

class Notifications extends Component
{

	/**
	 * @var Client
	 */
	public $twilioClient;


	/**
	 * Normalizes a phone number to standard USA (E.164) format, or returns null if the given number cannot be normalized.
	 */
	public function normalizeUsPhoneNumber(string $phoneNumber): ?string
	{

		$phoneNumber = (string)$phoneNumber;
		$phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

		// Make sure it starts with 1
		if (strlen($phoneNumber) < 11 && strpos($phoneNumber, '1') !== 0)
		{
			$phoneNumber = '1' . $phoneNumber;
		}

		// Add the + prefix
		$phoneNumber = '+' . $phoneNumber;

		// If the result is of the expected length, we're good; return.
		if (strlen($phoneNumber) === 12)
		{
			return $phoneNumber;
		}

		return null;

	}

	/**
	 * @return Client
	 *
	 * @throws \Twilio\Exceptions\ConfigurationException
	 */
	public function getTwilioClient()
	{

		if (!$this->twilioClient)
		{

			$sid = CommitteeTools::getInstance()->getSettings()->twilioSid;
			$token = CommitteeTools::getInstance()->getSettings()->twilioToken;

			$this->twilioClient = new Client($sid, $token);

		}

		return $this->twilioClient;

	}

	/**
	 * @throws \Twilio\Exceptions\ConfigurationException
	 *
	 * @todo Return void and use exceptions.
	 */
	public function sendSmsMessage(string $message, array $phoneNumbers = [], $includePrefix = true): bool
	{

		$settings = CommitteeTools::getInstance()->getSettings();

		if ($settings->silenceSmsNotifications)
		{
			$phoneNumbers = $settings->testSmsRecipients ?: [];
		}

		$errors = [];
		$phoneNumbers = (is_array($phoneNumbers) ? $phoneNumbers : [$phoneNumbers]);
		$normalizedNumbers = [];

		foreach ($phoneNumbers as $phoneNumber)
		{
			$normalizedNumber = $this->normalizeUsPhoneNumber($phoneNumber);
			if ($normalizedNumber)
			{
				$normalizedNumbers[] = $normalizedNumber;
			}
			else
			{
				$failedNumbers[] = $phoneNumber;
				$errors[] = 'Phone number cannot be normalized: ' . $phoneNumber;
			}
		}

		$phoneNumbers = array_unique($normalizedNumbers);

		if (empty($phoneNumbers))
		{
			$errors[] = 'No valid phone numbers.';
		}

		$twilio = $this->getTwilioClient();

		$successNumbers = [];
		$failedNumbers = [];

		foreach ($phoneNumbers as $phoneNumber)
		{

			try
			{

				$twilioMessage = $twilio->messages->create(
					$phoneNumber,
					[
						'body' => ($includePrefix ? $settings->smsPrefix : '') . $message,
						'from' => $settings->twilioFromNumber
					]
				);

				if (empty($twilioMessage->errorCode))
				{
					$successNumbers[] = $phoneNumber;
				}
				else
				{
					$failedNumbers[] = $phoneNumber;
					$errors[] = "Error sending SMS to {$phoneNumber}. " . $twilioMessage->errorMessage;
				}

			}
			catch (\Exception $e)
			{
				$failedNumbers[] = $phoneNumber;
				$errors[] = "Error sending SMS to {$phoneNumber}. " . $e->getMessage();
			}

		}

		array_walk($errors, [CommitteeTools::class, 'error']);

		// TODO: Archive sent message record.

		return empty($errors);

	}

	public function sendSmsToShiftAssignees(int $shiftId, string $message, bool $checkedInOnly = false)
	{

		$shift = CommitteeTools::getInstance()->shifts->getShiftById($shiftId);
		if (!$shift)
		{
			throw new \Exception("Shift not found.");
		}

		CommitteeTools::log("Sending SMS to " . ($checkedInOnly ? "checked-in members of " : "") . "Shift {$shiftId}: {$message}");

		$phoneNumbers = array_map(
			function(ShiftAssignmentRecord $assignment) use ($checkedInOnly) {

				// Maybe we don't care about check-in status...
				if (!$checkedInOnly)
				{
					return $assignment->assignedMember->getBestPhoneNumber();
				}

				// $checkedInOnly flag is set; only return if they're checked in.
				if (in_array(
					$assignment->status,
					[
						ShiftAssignmentRecord::STATUS_CHECKED_IN,
						ShiftAssignmentRecord::STATUS_CHECKED_IN_LATE,
						ShiftAssignmentRecord::STATUS_CHECKED_IN_EXCUSED,
					]
				))
				{
					return $assignment->assignedMember->getBestPhoneNumber();
				}

				// If we're here, they aren't checked in.
				return null;

			},
			CommitteeTools::getInstance()->assignments->getAssignmentsByShift($shift)
		);

		// We might have null returns from members who don't have a phone number or aren't checked in.
		$phoneNumbers = array_filter($phoneNumbers);

		try
		{
			if (!$this->sendSmsMessage($message, $phoneNumbers))
			{
				throw new \Exception("Error while sending SMS messages.");
			}
		}
		catch(\Exception $e)
		{
			CommitteeTools::error($e->getMessage());
			throw new \Exception("Error while sending SMS messages.");
		}

	}

	/**
	 * @param string|array $to
	 *
	 * @return bool
	 */
	public function sendEmailNotification(string $subject, $to, string $template, array $variables = [])
	{

		$message = \Craft::$app->getMailer()->compose();
		$settings = CommitteeTools::getInstance()->getSettings();

		if ($settings->silenceEmailNotifications)
		{
			$to = $settings->testEmailRecipients ?: [];
		}

		$message->setTo($to);

		if (!empty($settings->bccEmailRecipients))
		{
			$message->setBcc($settings->bccEmailRecipients);
		}

		$message->setSubject($settings->emailSubjectPrefix . $subject);

		try
		{
			$body = TemplateHelper::renderSystemTemplate($template, $variables);
			if (StringHelper::contains($body, 'html', false))
			{
				$message->setHtmlBody($body);
			}
			else
			{
				$message->setTextBody($body);
			}
		}
		catch(\Exception $e)
		{
			CommitteeTools::error($e->getMessage());
			return false;
		}

		return $message->send();

	}

}
