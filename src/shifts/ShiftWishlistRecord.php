<?php
namespace hlsr\committeetools\shifts;

use hlsr\committeetools\base\BaseRecord;

/**
 * @property $cn
 * @property $shiftId
 */
class ShiftWishlistRecord extends BaseRecord
{

	const TableName = 'hlsr_committeetools_shift_wishlists';

	public static function find(): ShiftWishlistQuery
	{
		return (new ShiftWishlistQuery(static::class));
	}

}
