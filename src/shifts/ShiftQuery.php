<?php
namespace hlsr\committeetools\shifts;

use craft\elements\db\ElementQuery;
use craft\helpers\Db;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\rosters\Subcommittee;
use yii\db\Connection;

/**
 * Represents a SQL query for Shift elements.
 *
 * @method ShiftElement[]|array all(Connection $db = null)
 * @method ShiftElement|array|null one(Connection $db = null)
 * @method ShiftElement|array|null nth(int $n, Connection $db = null)
 */
class ShiftQuery extends ElementQuery
{

	/**
	 * @var string
	 */
	public $uniqueKey;

	/**
	 * @var string
	 */
	public $displayName;

	/**
	 * @var string
	 */
	public $showYear;

	/**
	 * @var string
	 */
	public $assignedSubcommittee;

	/**
	 * @var string
	 */
	public $managedBySubcommittee;

	/**
	 * @var string
	 */
	public $startDate;

	/**
	 * @var string
	 */
	public $endDate;

	/**
	 * @var string
	 */
	public $division;

	/**
	 * @var string
	 */
	public $area;

	/**
	 * @var bool
	 */
	public $inCheckinWindow;

	/**
	 * @inheritdoc
	 */
	protected $defaultOrderBy = [ShiftRecord::TableName.'.startDate' => SORT_ASC];


	// Public Methods
	// =========================================================================

	public function uniqueKey(string $value = null)
	{
		$this->uniqueKey = $value;
		return $this;
	}

	public function displayName(string $value = null)
	{
		$this->displayName = $value;
		return $this;
	}

	public function showYear($value = null)
	{
		$this->showYear = $value;
		return $this;
	}

	public function inCurrentShowYear()
	{
		$this->showYear = CommitteeTools::getInstance()->getSettings()->showYear;
		return $this;
	}

	public function assignedSubcommittee($value = null)
	{
		$this->assignedSubcommittee = $value;
		return $this;
	}

	public function managedBySubcommittee($value = null)
	{
		$this->managedBySubcommittee = $value;
		return $this;
	}

	public function division($value = null)
	{
		$this->division = $value;
		return $this;
	}

	public function area($value = null)
	{
		$this->area = $value;
		return $this;
	}

	public function startDate($value = null)
	{
		$this->startDate = $value;
		return $this;
	}

	public function endDate($value = null)
	{
		$this->endDate = $value;
		return $this;
	}

	/**
	 * Selects shifts that are currently in their check-in window.
	 */
	public function inCheckinWindow(): self
	{
		$this->inCheckinWindow = true;
		return $this;
	}


	// Protected Methods
	// =========================================================================

	/**
	 * @inheritdoc
	 */
	protected function beforePrepare(): bool
	{

		$this->joinElementTable(ShiftRecord::TableName);

		$this->query->select([
			ShiftRecord::tableName().'.uniqueKey',
			ShiftRecord::tableName().'.showYear',
			ShiftRecord::tableName().'.assignedSubcommitteeNames',
			ShiftRecord::tableName().'.managedBySubcommitteeNames',
			ShiftRecord::tableName().'.startDate',
			ShiftRecord::tableName().'.endDate',
			ShiftRecord::tableName().'.displayName',
			ShiftRecord::tableName().'.eventName',
			ShiftRecord::tableName().'.division',
			ShiftRecord::tableName().'.area',
			ShiftRecord::tableName().'.meta',
		]);

		if ($this->uniqueKey) {
			$this->subQuery->andWhere([ShiftRecord::tableName().'.uniqueKey' => $this->uniqueKey]);
		}

		if ($this->displayName) {
			$this->subQuery->andWhere(Db::parseParam(ShiftRecord::tableName().'.displayName', $this->displayName));
		}

		if ($this->showYear) {
			$this->subQuery->andWhere([ShiftRecord::tableName().'.showYear' => $this->showYear]);
		}

		if ($this->assignedSubcommittee) {
			$condition = $this->parseSubcommitteeCondition(
				ShiftRecord::tableName().'.assignedSubcommitteeNames',
				$this->assignedSubcommittee
			);
			$this->subQuery->andWhere($condition);
		}

		if ($this->managedBySubcommittee) {
			$condition = $this->parseSubcommitteeCondition(
				ShiftRecord::tableName().'.managedBySubcommitteeNames',
				$this->managedBySubcommittee
			);
			$this->subQuery->andWhere($condition);
		}

		if ($this->startDate) {
			$this->subQuery->andWhere(Db::parseDateParam(ShiftRecord::tableName().'.startDate', $this->startDate));
		}

		if ($this->endDate) {
			$this->subQuery->andWhere(Db::parseDateParam(ShiftRecord::tableName().'.endDate', $this->endDate));
		}

		if ($this->division) {
			$this->subQuery->andWhere([ShiftRecord::tableName().'.division' => $this->division]);
		}

		if ($this->area) {
			$this->subQuery->andWhere([ShiftRecord::tableName().'.area' => $this->area]);
		}

		/*
		 * The check-in window for each shift extends from two hours before, to the scheduled end time.
		 */
		if ($this->inCheckinWindow)
		{
			$this->subQuery->andWhere(Db::parseDateParam('DATE_SUB(' . ShiftRecord::tableName() . '.startDate, INTERVAL 2 HOUR)', '< now'));
			$this->subQuery->andWhere(Db::parseDateParam(ShiftRecord::tableName().'.endDate', '> now'));
		}

		return parent::beforePrepare();

	}

	/**
	 * @param string|array|Subcommittee $value
	 * @param string $column
	 *
	 * @return array
	 */
	public function parseSubcommitteeCondition($column, $value)
	{

		// Is it already a Subcommittee? We're done.

		if ($value instanceof Subcommittee)
		{
			return ['REGEXP', $column, ($value->name ?: '---') . '(,.*)?$'];
		}

		// Normalize to an array

		if (is_string($value))
		{
			$value = CommitteeTools::getInstance()->subcommittees->getSubcommitteesFromNames($value);
		}

		// Get the condition

		if (is_array($value) && count($value) > 0)
		{

			$regexpConditions = array_map(
				function($subcommittee) use ($column)
				{
					if ($subcommittee instanceof Subcommittee)
					{
						return ['REGEXP', $column, ($subcommittee->name ?: '---') . '(,.*)?$'];
					}
					return ['REGEXP', $column, ($subcommittee ?: '---') . '(,.*)?$'];
				},
				$value
			);

			if(count($regexpConditions) == 1)
			{
				return array_pop($regexpConditions);
			}

			return array_merge(['or'], $regexpConditions);

		}

		return null;

	}

}
