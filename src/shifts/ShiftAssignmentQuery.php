<?php
namespace hlsr\committeetools\shifts;

use craft\helpers\DateTimeHelper;
use craft\helpers\Db;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\members\MembersHelper;
use yii\db\ActiveQuery;
use yii\db\Connection;

class ShiftAssignmentQuery extends ActiveQuery
{

	public function init()
	{
		$this->select([
			ShiftAssignmentRecord::tableName().'.*',
		])
			->innerJoinWith('shift', true)
			->joinWith('assignedMember', true);
	}

	/**
	 * @param MemberRecord|int $member
	 */
	public function assignedTo($member): self
	{
		// If we don't have a Member/CN, fall back to a zero for the query to prevent weird results from vacant assignments.
		$cn = MembersHelper::normalizeMemberToCn($member) ?: 0;
		return $this
			->andWhere([ShiftAssignmentRecord::tableName().'.assignedMemberCn' => $cn]);
	}

	/**
	 * @param $value
	 *
	 * @return ShiftAssignmentQuery
	 */
	public function startDate($value): self
	{
		// TODO: Remove superfluous addSelect?
		return $this
			->addSelect([ShiftRecord::tableName().'.startDate'])
			->andWhere(Db::parseDateParam(ShiftRecord::tableName().'.startDate', $value));
	}

	/**
	 * @param $value
	 *
	 * @return ShiftAssignmentQuery
	 */
	public function showYear($value)
	{
		// TODO: Remove superfluous addSelect?
		return $this
			->addSelect([ShiftRecord::tableName().'.showYear'])
			->andWhere([ShiftRecord::tableName().'.showYear' => $value]);
	}

	/**
	 * @return ShiftAssignmentQuery
	 */
	public function inCurrentShowYear()
	{
		return $this->showYear(CommitteeTools::getInstance()->getSettings()->showYear);
	}

	/**
	 * @param ShiftElement|ShiftRecord|int|int[] $shift
	 *
	 * @return ShiftAssignmentQuery
	 */
	public function forShift($shift)
	{
		// TODO: Array support?
		if ($shift instanceof ShiftElement || $shift instanceof ShiftRecord)
		{
			$shift = $shift->id;
		}
		return $this->shiftId($shift);
	}

	/**
	 * @param int|int[] $shiftId
	 */
	public function shiftId($shiftId): self
	{
		return $this->andWhere([ShiftAssignmentRecord::tableName().'.shiftId' => $shiftId]);
	}

	public function availableForPickup(): self
	{
		return $this->andWhere([ShiftAssignmentRecord::tableName().'.availableForPickup' => true]);
	}

	public function availableForTrade(): self
	{
		return $this->andWhere([ShiftAssignmentRecord::tableName().'.availableForTrade' => true]);
	}

	public function hasAssignedMember(): self
	{
		return $this->andWhere(['not', [ShiftAssignmentRecord::tableName().'.assignedMemberCn' => null]]);
	}

	/**
	 * @return ShiftAssignmentQuery
	 *
	 * @throws \Exception
	 */
	public function onDay($date)
	{
		$date = DateTimeHelper::toDateTime($date);
		$today = (clone $date)->setTime(0,0,0);
		$tomorrow = (clone $date)->modify('+1 day')->setTime(0,0,0);
		return $this
			->addSelect([ShiftRecord::tableName().'.startDate'])
			->addSelect([ShiftRecord::tableName().'.endDate'])
			->andWhere(['>=', ShiftRecord::tableName().'.startDate', Db::prepareDateForDb($today)])
			->andWhere(['<', ShiftRecord::tableName().'.endDate', Db::prepareDateForDb($tomorrow)]);
	}

	/**
	 * @return ShiftAssignmentQuery
	 *
	 * @throws \Exception
	 */
	public function today()
	{
		return $this->onDay(time());
	}

	/**
	 * @param Connection $db the DB connection used to create the DB command.
	 *
	 * @return ShiftAssignmentRecord[]
	 */
	public function all($db = null): array
	{
		return parent::all($db);
	}

	/**
	 * @param Connection $db the DB connection used to create the DB command.
	 *
	 * @return int[]
	 */
	public function shiftIds($db = null): array
	{
		return $this->select('shiftId')->column($db);
	}

}
