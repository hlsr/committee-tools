<?php
namespace hlsr\committeetools\shifts;

use craft\db\Query;
use craft\helpers\DateTimeHelper;
use hlsr\committeetools\checkins\CheckinRecord;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\spreadsheets\SpreadsheetsHelper;
use yii\base\Component;

/**
 * TODO: Listen for Member deletion and soft-delete any Shift Assignments they're assigned to.
 */
class ShiftAssignments extends Component
{

	const CHECKED_IN_STATUSES = [
		ShiftAssignmentRecord::STATUS_CHECKED_IN,
		ShiftAssignmentRecord::STATUS_CHECKED_IN_LATE,
		ShiftAssignmentRecord::STATUS_CHECKED_IN_EXCUSED,
	];

	const IMPORT_ACTION_ASSIGN = 'assign';
	const IMPORT_ACTION_REMOVE = 'remove';

	/**
	 * @param int $id
	 *
	 * @return ShiftAssignmentRecord|null
	 */
	public function getAssignmentById($id = 0)
	{
		return ShiftAssignmentRecord::findOne($id);
	}

	/**
	 * @return ShiftAssignmentQuery
	 */
	public function find()
	{
		return ShiftAssignmentRecord::find();
	}

	/**
	 * @param $shift
	 *
	 * @return ShiftAssignmentRecord[]
	 */
	public function getAssignmentsByShift($shift)
	{
		return ShiftAssignmentRecord::find()->forShift($shift)->all();
	}

	/**
	 * @param $member
	 * @param bool $currentShowYear
	 *
	 * @return ShiftAssignmentRecord[]
	 */
	public function getAssignmentsByMember($member, $currentShowYear = true)
	{

		$assignmentsQuery = ShiftAssignmentRecord::find()->assignedTo($member);

		if ($currentShowYear)
		{
			$assignmentsQuery->inCurrentShowYear();
		}

		$assignmentsQuery->orderBy(ShiftRecord::tableName().'.startDate asc');

		return $assignmentsQuery->all();

	}

	public function createSubcommitteeAssignmentsForAllShifts()
	{

		$success = true;

		$shifts = ShiftElement::find()
			->inCurrentShowYear()
			->all();

		CommitteeTools::log("Creating Assignments for " . count($shifts) . " Shifts.");

		foreach($shifts as $shift)
		{
			if ($shift->assignedSubcommitteeNames)
			{
				try
				{
					$this->createSubcommitteeAssignmentsForShift($shift);
				}
				catch (\Exception $e)
				{
					$success = false;
				}
			}
		}

		if (!$success)
		{
			throw new \Exception("Some assignments could not be created.");
		}

	}

	public function createSubcommitteeAssignmentsForShift(ShiftElement $shift)
	{

		$success = true;

		$subcommittees = CommitteeTools::getInstance()->subcommittees->getSubcommitteesFromNames($shift->assignedSubcommitteeNames);

		foreach ($subcommittees as $subcommittee)
		{

			$subcommitteeMembers = CommitteeTools::getInstance()->members->getMembersBySubcommittee($subcommittee);
			CommitteeTools::log("Assigning " . count($subcommitteeMembers) . " members from {$subcommittee->name} to Shift {$shift->id} ({$shift->displayName}).");

			foreach ($subcommitteeMembers as $member)
			{
				try
				{
					$this->createMemberAssignmentForShift(
						$member,
						$shift,
						['type' => ShiftAssignmentRecord::TYPE_SUBCOMMITTEE],
					);
				}
				catch (\Exception $e)
				{
					CommitteeTools::error($e->getMessage());
					$success = false;
				}
			}

		}

		if (!$success)
		{
			throw new \Exception("Some assignments could not be created.");
		}

	}

	public function createMemberAssignmentForShift(MemberRecord $member, ShiftElement $shift, array $extraAttributes = [], bool $force = false)
	{

		/*
		 * Using a Query here instead of an active-record query, because
		 * (a) the performance is better, and
		 * (b) the performance is extra-better because apparently SoftDeleteBehavior makes everything hella slow
		 */
		$skipBecauseAlreadyAssigned = !$force && (new Query())
			->from(ShiftAssignmentRecord::tableName())
			->where(['shiftId' => $shift->id])
			->andWhere([
				'or',
				['assignedMemberCn' => $member->cn],
				['originalAssignedMemberCn' => $member->cn]
			])
			->exists();

		if ($skipBecauseAlreadyAssigned)
		{
			return;
		}

		$attributes = [
			'shiftId' => $shift->id,
			'assignedMemberCn' => $member->cn,
			'originalAssignedMemberCn' => $member->cn,
			'type' => ShiftAssignmentRecord::TYPE_INDIVIDUAL,
			'meta' => ['assignedSubcommitteeNames' => $shift->assignedSubcommitteeNames]
		];

		$attributes = array_merge($attributes, $extraAttributes);

		$assignment = new ShiftAssignmentRecord($attributes);

		if (!$assignment->save())
		{
			throw new \Exception("Error assigning Shift {$shift->id} to Member {$member->cn}");
		}

	}

	public function createUnassignedAssignmentsForShift(ShiftElement $shift, int $qty, $extraAttributes = [])
	{

		if ($qty < 1)
		{
			return;
		}

		$attributes = [
			'shiftId' => $shift->id,
		];

		$attributes = array_merge($attributes, $extraAttributes);

		foreach (range(1, $qty) as $i)
		{
			$assignment = new ShiftAssignmentRecord($attributes);
			if (!$assignment->save())
			{
				throw new \Exception("Could not save Shift Assignment {$i} of {$qty}.");
			}
		}

	}

	/**
	 * @param ShiftAssignmentRecord $assignment
	 * @param MemberRecord $member
	 *
	 * @return ShiftAssignmentRecord[]
	 */
	public function getTradeEligibleAssignments(ShiftAssignmentRecord $assignment)
	{

		if (is_callable(CommitteeTools::getInstance()->getSettings()->getTradeEligibleAssignments))
		{
			return (CommitteeTools::getInstance()->getSettings()->getTradeEligibleAssignments)($assignment);
		}

		return [];

	}

	/**
	 * @param MemberRecord $member
	 *
	 * @return ShiftAssignmentRecord[]
	 */
	public function getPickupEligibleAssignments(MemberRecord $member)
	{

		if (is_callable(CommitteeTools::getInstance()->getSettings()->getPickupEligibleAssignments))
		{
			return (CommitteeTools::getInstance()->getSettings()->getPickupEligibleAssignments)($member);
		}

		return [];

	}

	/**
	 * @param ShiftAssignmentRecord $assignment
	 *
	 * @return array
	 */
	public function getAssignmentAreaOptions(ShiftAssignmentRecord $assignment)
	{

		if (is_callable(CommitteeTools::getInstance()->getSettings()->getAssignmentAreaOptions))
		{
			return (CommitteeTools::getInstance()->getSettings()->getAssignmentAreaOptions)($assignment);
		}

		return [];

	}

	/**
	 * @return array
	 */
	public static function getAssignmentStatusOptions()
	{
		return [
			ShiftAssignmentRecord::STATUS_CHECKED_IN => 'Checked In',
			ShiftAssignmentRecord::STATUS_CHECKED_IN_LATE => 'Checked In, Late',
			// ShiftAssignmentRecord::STATUS_CHECKED_IN_EXCUSED => 'Checked In - Excused',
			ShiftAssignmentRecord::STATUS_DISMISSED => 'Dismissed',
			ShiftAssignmentRecord::STATUS_EXCUSED => 'Excused',
		];
	}

	/**
	 * @param ShiftAssignmentRecord $assignment
	 *
	 * @return bool
	 */
	public function refreshPendingRequestsStatus(ShiftAssignmentRecord $assignment)
	{

		$pendingTradeRequestsCount = ShiftAssignmentRequestRecord::find()
			->isPending()
			->involvingShiftAssignment($assignment)
			->type(ShiftAssignmentRequestRecord::TYPE_TRADE)
			->count();
		$assignment->tradePending = ($pendingTradeRequestsCount > 0);

		$pendingPickupRequestsCount = ShiftAssignmentRequestRecord::find()
			->isPending()
			->involvingShiftAssignment($assignment)
			->type(ShiftAssignmentRequestRecord::TYPE_PICKUP)
			->count();
		$assignment->pickupPending = ($pendingPickupRequestsCount > 0);

		return $assignment->save();

	}

	/**
	 * @param ShiftAssignmentRecord $assignment
	 * @param null $location
	 * @param bool $save
	 *
	 * @return bool
	 *
	 * @throws \Exception
	 */
	public function checkInAssignment(ShiftAssignmentRecord $assignment, $location = null, $save = true)
	{

		// Only do any of this is the Assignment isn't checked in yet.

		if (empty($assignment->status) && empty($assignment->checkedInTime))
		{

			$statusUpdateRecord = new ShiftAssignmentStatusUpdateRecord([
				'shiftAssignmentId' => $assignment->id,
				'status' => ShiftAssignmentRecord::STATUS_CHECKED_IN,
				'updatedFromLocation' => $location,
			]);
			$statusUpdateRecord->save();

			$assignment->status = ShiftAssignmentRecord::STATUS_CHECKED_IN;
			$assignment->checkedInTime = DateTimeHelper::toDateTime(time());
			return !$save || $assignment->save();

		}

		return true;

	}

	public function importFromFile(string $file)
	{

		// Make sure the file path exists
		if (!file_exists($file))
		{
			throw new \Exception("The specified file does not exist: {$file}");
		}

		$data = SpreadsheetsHelper::getRowsFromFile($file, true);

		$this->processImportedAssignmentsData($data);

	}

	public function processImportedAssignmentsData(array $data)
	{

		$success = true;

		foreach ($data as $row)
		{

			try
			{

				$cn = $row['CUSTOMER_NUMBER'] ?? null;
				$action = $row['ACTION'] ?? null;
				$shiftKey = $row['SHIFT_KEY'] ?? null;
				$shiftName = $row['SHIFT_NAME'] ?? null;

				if (!$cn || !$action)
				{
					throw new \Exception("CUSTOMER_NUMBER and assignment ACTIOM are required.");
				}

				if (!$shiftKey && !$shiftName)
				{
					throw new \Exception("Either SHIFT_KEY or SHIFT_NAME are required.");
				}

				$member = CommitteeTools::getInstance()->members->getMemberByCn((int) $cn);

				if (!$member)
				{
					throw new \Exception("Couldn't find member: {$cn}");
				}

				$shiftQuery = ShiftElement::find()
					->inCurrentShowYear();

				if ($shiftKey)
				{
					$shiftQuery->uniqueKey($shiftKey);
				}

				if ($shiftName)
				{
					$shiftQuery->displayName($shiftName);
				}

				$shift = $shiftQuery->one();

				if (!$shift)
				{
					throw new \Exception("Couldn't find shift in current Show Year: " . ($shiftKey ?? $shiftName));
				}

				if ($action === self::IMPORT_ACTION_ASSIGN)
				{

					$attributes = !empty($row['POSITION']) ? ['position' => $row['POSITION']] : [];
					CommitteeTools::log("Assigning member {$cn} to shift: {$shift->displayName}");
					CommitteeTools::getInstance()->assignments->createMemberAssignmentForShift($member, $shift, $attributes);
					// TODO: Should we be forcing this even if a member was previously assigned?

				}

				if ($action === self::IMPORT_ACTION_REMOVE)
				{

					$assignment = ShiftAssignmentRecord::find()
						->assignedTo($cn)
						->forShift($shift)
						->inCurrentShowYear()
						->one();
					if ($assignment)
					{
						CommitteeTools::log("Removing member {$cn} from shift: {$shift->displayName}");
						$assignment->trash();
					}

				}

				// Free up memory, maybe?
				unset($member, $shiftQuery, $shift);

			}
			catch (\Exception $e)
			{
				CommitteeTools::error($e->getMessage());
				$success = false;
			}

		}

		if (!$success)
		{
			throw new \Exception("Error while importing shift assignments.");
		}

	}

	/**
	 * @return array
	 */
	public function shame_getAllCheckins()
	{
		return CheckinRecord::find()->select('cn')->andWhere(['>', 'dateCreated', date('Y-m-d')])->asArray(true)->column();
	}

}
