<?php
namespace hlsr\committeetools\shifts;

use craft\records\User;
use hlsr\committeetools\base\BaseRecord;
use hlsr\committeetools\base\TrashableTrait;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;

/**
 * @property $id
 * @property $shiftId
 * @property $assignedMemberCn
 * @property $originalAssignedMemberCn
 * @property $type
 * @property $area
 * @property $position
 * @property $status
 * @property $availableForTrade
 * @property $tradePending
 * @property $tradeCompleted
 * @property $availableForPickup
 * @property $pickupPending
 * @property $pickupCompleted
 * @property $locked
 * @property $checkedInTime
 * @property $checkedInByUserId
 * @property $checkedOutTime
 * @property $checkedOutByUserId
 * @property $note
 * @property $meta
 *
 * @property MemberRecord $assignedMember
 * @property ShiftRecord $shift
 *
 */
class ShiftAssignmentRecord extends BaseRecord
{

	use TrashableTrait;

	const TableName = 'hlsr_committeetools_shift_assignments';

	const TYPE_COMMITTEE = 'committee';
	const TYPE_INDIVIDUAL = 'individual';
	const TYPE_PICKUP = 'pickup';
	const TYPE_SUBCOMMITTEE = 'subcommittee';

	const STATUS_CHECKED_IN = 'checkedIn';
	const STATUS_CHECKED_IN_LATE = 'checkedInLate';
	const STATUS_CHECKED_IN_EXCUSED = 'checkedInExcused';
	const STATUS_DISMISSED = 'dismissed';
	const STATUS_EXCUSED = 'excused';

	protected $dateTimeAttributes = [
		'checkedInTime',
		'checkedOutTime',
		'dateCreated',
		'dateUpdated',
	];

	public static function find(): ShiftAssignmentQuery
	{
		return (new ShiftAssignmentQuery(static::class))
			->where(['dateDeleted' => null]);
	}

	public static function findTrashed(): ShiftAssignmentQuery
	{
		return (new ShiftAssignmentQuery(static::class))
			->where(['not', ['dateDeleted' => null]]);
	}

	public static function findWithTrashed(): ShiftAssignmentQuery
	{
		return new ShiftAssignmentQuery(static::class);
	}

	public function getAssignedMember(): \yii\db\ActiveQuery
	{
		return $this->hasOne(MemberRecord::class, ['cn' => 'assignedMemberCn']);
	}

	/*
	 * @TODO: Preferably, change this schema to use Member record instead
	 */
	public function getCheckedInByUser(): \yii\db\ActiveQuery
	{
		return $this->hasOne(User::class, ['id' => 'checkedInByUserId']);
	}

	/**
	 * @return ShiftAssignmentRequestRecord[]
	 */
	public function getPendingTradeRequests(): array
	{
		return CommitteeTools::getInstance()->assignmentRequests->getPendingTradeRequestsForAssignment($this);
	}

	public function getShift(): \yii\db\ActiveQuery
	{
		return $this->hasOne(ShiftRecord::class, ['id' => 'shiftId']);
	}



}
