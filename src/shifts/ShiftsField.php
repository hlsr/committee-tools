<?php
namespace hlsr\committeetools\shifts;

use Craft;
use craft\fields\BaseRelationField;

class ShiftsField extends BaseRelationField
{

	// Static
	// =========================================================================

	/**
	 * @inheritdoc
	 */
	public static function displayName(): string
	{
		return Craft::t('committee-tools', 'Shifts');
	}

	/**
	 * @inheritdoc
	 */
	protected static function elementType(): string
	{
		return ShiftElement::class;
	}

	/**
	 * @inheritdoc
	 */
	public static function defaultSelectionLabel(): string
	{
		return Craft::t('committee-tools', 'Add an Shift');
	}

	/**
	 * @inheritdoc
	 */
	public static function valueType(): string
	{
		return ShiftQuery::class;
	}

}
