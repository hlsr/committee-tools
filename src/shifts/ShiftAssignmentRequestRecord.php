<?php
namespace hlsr\committeetools\shifts;

use craft\records\User;
use hlsr\committeetools\base\BaseRecord;
use hlsr\committeetools\CommitteeTools;

/**
 * @property int $id
 * @property int $sourceShiftAssignmentId
 * @property int $targetShiftAssignmentId
 * @property string $type
 * @property string $status
 * @property \DateTime $expiryDate
 * @property \DateTime $resolvedDate
 * @property int $initiatingUserId
 * @property int $resolvingUserId
 */
class ShiftAssignmentRequestRecord extends BaseRecord
{

	const TableName = 'hlsr_committeetools_shift_assignment_requests';

	const STATUS_APPROVED = 'approved';
	const STATUS_CANCELLED = 'cancelled';
	const STATUS_DECLINED = 'declined';
	const STATUS_EXPIRED = 'expired';
	const STATUS_PENDING = 'pending';
	const STATUS_VOIDED = 'voided';

	const TYPE_PICKUP = 'pickup';
	const TYPE_TRADE = 'trade';

	/**
	 * @var ShiftAssignmentRecord|null
	 */
	private $_sourceAssignment;

	/**
	 * @var ShiftAssignmentRecord|null
	 */
	private $_targetAssignment;

	/**
	 * @return ShiftAssignmentRequestQuery
	 */
	public static function find()
	{
		return new ShiftAssignmentRequestQuery(static::class);
	}

	/**
	 * @return bool
	 */
	public function isResolved()
	{
		return ($this->status != self::STATUS_PENDING);
	}

	/**
	 * TODO ?: Deprecate getSourceAssignment()
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getSourceShiftAssignment()
	{
		return $this->hasOne(ShiftAssignmentRecord::class, ['id' => 'sourceShiftAssignmentId']);
	}

	/**
	 * TODO ?: Deprecate getTargetAssignment()
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getTargetShiftAssignment()
	{
		return $this->hasOne(ShiftAssignmentRecord::class, ['id' => 'targetShiftAssignmentId']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getInitiatingUser()
	{
		return $this->hasOne(User::class, ['id' => 'initiatingUserId']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getResolvingUser()
	{
		return $this->hasOne(User::class, ['id' => 'resolvingUserId']);
	}

	/* -- */

	/**
	 * @return ShiftAssignmentRecord|null
	 *
	 * @deprecated
	 */
	public function getSourceAssignment()
	{
		if (empty($this->_sourceAssignment))
		{
			$this->_sourceAssignment = CommitteeTools::getInstance()->assignments->getAssignmentById($this->sourceShiftAssignmentId);
		}
		return $this->_sourceAssignment;
	}

	/**
	 * @return ShiftAssignmentRecord|null
	 *
	 * @deprecated
	 */
	public function getTargetAssignment()
	{
		if (empty($this->_targetAssignment))
		{
			$this->_targetAssignment = CommitteeTools::getInstance()->assignments->getAssignmentById($this->targetShiftAssignmentId);
		}
		return $this->_targetAssignment;
	}

}
