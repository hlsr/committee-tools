<?php
namespace hlsr\committeetools\shifts;

use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\members\MembersHelper;
use yii\db\ActiveQuery;
use yii\db\Connection;

class ShiftWishlistQuery extends ActiveQuery
{

	public function cn($cn): self
	{
		return $this->andWhere(['cn' => $cn]);
	}

	/**
	 * @param MemberRecord|int|int[] $member
	 */
	public function forMember($member): self
	{
		// TODO: Array support?
		$cn = MembersHelper::normalizeMemberToCn($member);
		return $this->cn($cn);
	}

	public function shiftId($shiftId): self
	{
		return $this->andWhere(['shiftId' => $shiftId]);
	}

	/**
	 * @param ShiftElement|ShiftRecord|int|int[] $shift
	 */
	public function forShift($shift): self
	{
		// TODO: Array support?
		if ($shift instanceof ShiftElement || $shift instanceof ShiftRecord)
		{
			$shift = $shift->id;
		}
		return $this->shiftId($shift);
	}

	/**
	 * @param Connection $db
	 * @return ShiftAssignmentRecord[]
	 */
	public function all($db = null): array
	{
		return parent::all($db);
	}

	/**
	 * @param Connection $db
	 * @return int[]
	 */
	public function cns($db = null): array
	{
		return $this->select('cn')->column($db);
	}

	/**
	 * @param Connection $db
	 * @return int[]
	 */
	public function shiftIds($db = null): array
	{
		return $this->select('shiftId')->column($db);
	}

}
