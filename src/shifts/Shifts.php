<?php
namespace hlsr\committeetools\shifts;

use Craft;
use craft\helpers\DateTimeHelper;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\spreadsheets\SpreadsheetsHelper;
use yii\base\Component;
use yii\base\InvalidArgumentException;

class Shifts extends Component
{

	public function getShiftById(int $id = 0): ?ShiftElement
	{
		return Craft::$app->elements->getElementById($id);
	}

	/**
	 * Returns a new Shift query
	 *
	 * @param array $criteria
	 *
	 * @return ShiftQuery
	 */
	public function find(array $criteria = []): ShiftQuery
	{
		$query = ShiftElement::find();
		Craft::configure($query, $criteria);
		return $query;
	}

	/**
	 * @param MemberRecord $member
	 *
	 * @return ShiftElement[]
	 */
	public function getShiftsManagedByMember(MemberRecord $member, $currentShowYear = true)
	{

		$subcommittees = $member->getSubcommittees();

		if (empty($subcommittees))
		{
			return [];
		}

		$shiftQuery = ShiftElement::find()->managedBySubcommittee($subcommittees);

		if ($currentShowYear)
		{
			$shiftQuery->inCurrentShowYear();
		}

		$shiftQuery->orderBy(ShiftRecord::tableName().'.startDate asc');

		return $shiftQuery->all();

	}

	/**
	 * @param ShiftRecord|ShiftElement $shift
	 *
	 * @return MemberRecord[]
	 */
	public function getManagingMembersForShift($shift)
	{

		if (empty($shift->managedBySubcommitteeNames))
		{
			return [];
		}

		/** @var ShiftRecord|ShiftElement $shift */

		// TODO: Handle case where multiple Subcommittees are managing a Shift.

		// TODO: `isCaptain` should be a scope function on MemberQuery, not here
		// TODO: ...and anyway, should probably be isManagement, adjustable by config.

		return MemberRecord::find()
			->onSubcommittee($shift->managedBySubcommitteeNames)
			->andWhere(['description' => 'CAPTAIN'])
			->all();

	}

	public function getWishlistEligibleShifts(MemberRecord $member): array
	{

		if (is_callable(CommitteeTools::getInstance()->getSettings()->getWishlistEligibleShifts))
		{
			return (CommitteeTools::getInstance()->getSettings()->getWishlistEligibleShifts)($member);
		}

		return [];

	}

	/**
	 * @param string $file
	 *
	 * @return bool
	 *
	 * @throws InvalidArgumentException
	 * @throws \yii\base\ErrorException
	 */
	public function importFromFile(string $file)
	{

		// Make sure the file path exists
		if (!file_exists($file))
		{
			throw new InvalidArgumentException("The specified file does not exist: {$file}");
		}

		try
		{
			$data = SpreadsheetsHelper::getRowsFromFile($file, true);
		}
		catch(\Throwable $e)
		{
			CommitteeTools::error($e->getMessage());
			throw new InvalidArgumentException("Can't get data from file: {$file}");
		}

		$success = true;

		foreach ($data as $row)
		{

			try
			{

				$attributes = [
					'id' => isset($row['ID']) ? (int) $row['ID'] : null,
					'uniqueKey' => $row['UNIQUE_KEY'] ?? null,
					'showYear' => $row['SHOW_YEAR'] ?? null,
					'assignedSubcommitteeNames' => $row['ASSIGNED_SUBCOMMITTEE_NAMES'] ?? null,
					'managedBySubcommitteeNames' => $row['MANAGED_BY_SUBCOMMITTEE_NAMES'] ?? null,
					'startDate' => isset($row['START_DATE']) ? DateTimeHelper::toDateTime($row['START_DATE'], true) : null,
					'endDate' => isset($row['END_DATE']) ? DateTimeHelper::toDateTime($row['END_DATE'], true) : null,
					'displayName' => $row['DISPLAY_NAME'] ?? null,
					'eventName' => $row['EVENT_NAME'] ?? null,
					'division' => $row['DIVISION'] ?? null,
					'area' => $row['AREA'] ?? null,
				];

				if (empty($attributes['uniqueKey']))
				{
					throw new InvalidArgumentException("No Unique Key provided for importing Shift record.");
				}

				if (empty($attributes['showYear']))
				{
					throw new InvalidArgumentException("No Show Year provided for importing Shift record.");
				}

				// Try to find an existing Shift by Unique Key
				$element = ShiftElement::find()->uniqueKey($attributes['uniqueKey'])->one();

				// No existing Shift found.
				if (!$element)
				{
					$element = new ShiftElement();
				}

				unset($attributes['id']);
				$element->setAttributes($attributes, false);

				$success = Craft::$app->elements->saveElement($element) && $success;

			}
			catch (\Throwable $e)
			{
				CommitteeTools::error($e->getMessage());
				$success = false;
			}

		}

		return $success;

	}

}
