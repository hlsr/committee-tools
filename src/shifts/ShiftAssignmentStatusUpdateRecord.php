<?php
namespace hlsr\committeetools\shifts;

use hlsr\committeetools\base\BaseRecord;

/**
 * @property $id
 * @property $shiftAssignmentId
 * @property $status
 * @property $updatedByUserId
 * @property $updatedFromLocation
 * @property $comments
 */
class ShiftAssignmentStatusUpdateRecord extends BaseRecord
{

	const TableName = 'hlsr_committeetools_shift_assignment_status_updates';

}
