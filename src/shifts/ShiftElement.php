<?php
namespace hlsr\committeetools\shifts;

use Craft;
use craft\base\Element;
use craft\elements\db\ElementQueryInterface;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\rosters\Subcommittee;
use yii\base\Exception;

/**
 * @property $id
 * @property $uniqueKey
 * @property $showYear
 * @property $assignedSubcommitteeNames
 * @property $managedBySubcommitteeNames
 * @property $startDate
 * @property $endDate
 * @property $displayName
 * @property $eventName
 * @property $division
 * @property $area
 * @property $meta
 */
class ShiftElement extends Element
{

	/**
	 * @var string
	 */
	public $uniqueKey;

	/**
	 * @var int
	 */
	public $showYear;

	/**
	 * @var string
	 */
	public $assignedSubcommitteeNames;

	/**
	 * @var string
	 */
	public $managedBySubcommitteeNames;

	/**
	 * @var \DateTime
	 */
	public $startDate;

	/**
	 * @var \DateTime
	 */
	public $endDate;

	/**
	 * @var string
	 */
	public $displayName;

	/**
	 * @var string
	 */
	public $eventName;

	/**
	 * @var string
	 */
	public $division;

	/**
	 * @var string
	 */
	public $area;

	/**
	 * @var mixed
	 */
	public $meta;


	// Public Methods
	// =========================================================================

	/**
	 * @inheritdoc
	 */
	public function datetimeAttributes(): array
	{
		$attributes = parent::datetimeAttributes();
		$attributes[] = 'startDate';
		$attributes[] = 'endDate';
		return $attributes;
	}

	/**
	 * @inheritdoc
	 *
	 * @return ShiftQuery
	 */
	public static function find(): ElementQueryInterface
	{
		return new ShiftQuery(static::class);
	}

	/**
	 * @inheritdoc
	 */
	protected static function defineSources(string $context = null): array
	{

		$sources = [
			[
				'key' => '*',
				'label' => Craft::t('committee-tools', 'All Shifts'),
				'criteria' => [],
				'defaultSort' => ['statDate', 'asc']
			]
		];

		$thisYear = (int)CommitteeTools::getInstance()->getSettings()->showYear;
		$sources[] =
			[
				'key' => (string)$thisYear,
				'label' => (string)$thisYear,
				'criteria' => [
					'showYear' => $thisYear,
				],
				'defaultSort' => ['statDate', 'asc']
			];

		$lastYear = (int)CommitteeTools::getInstance()->getSettings()->showYear - 1;
		$sources[] =
			[
				'key' => (string)$lastYear,
				'label' => (string)$lastYear,
				'criteria' => [
					'showYear' => $lastYear,
				],
				'defaultSort' => ['statDate', 'asc']
			];

		return $sources;
	}

	/**
	 * @inheritdoc
	 */
	protected static function defineTableAttributes(): array
	{

		$attributes = [
			'id' => ['label' => Craft::t('app', 'ID')],
			'displayName' => ['label' => Craft::t('committee-tools', 'Display Name')],
			'startDate' => ['label' => Craft::t('app', 'Start Date')],
			'endDate' => ['label' => Craft::t('app', 'End Date')],
		];

		$attributes['dateCreated'] = ['label' => Craft::t('app', 'Date Created')];
		$attributes['dateUpdated'] = ['label' => Craft::t('app', 'Date Updated')];

		return $attributes;

	}

	/**
	 * @inheritdoc
	 */
	protected static function defineDefaultTableAttributes(string $source): array
	{
		return [
			'displayName',
			'startDate',
			'endDate',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getUrl(): string
	{
		return '';
	}

	/**
	 * @throws Exception
	 */
	public function afterSave(bool $isNew)
	{

		if (!$isNew)
		{

			// Try to find the record by ID
			$record = ShiftRecord::findOne($this->id);

			if (!$record)
			{
				throw new Exception('Invalid Shift ID: ' . $this->id);
			}

		}
		else
		{
			$record = new ShiftRecord();
			$record->id = $this->id;
		}

		$record->uniqueKey = $this->uniqueKey;
		$record->showYear = $this->showYear;
		$record->assignedSubcommitteeNames = $this->assignedSubcommitteeNames;
		$record->managedBySubcommitteeNames = $this->managedBySubcommitteeNames;
		$record->startDate = $this->startDate;
		$record->endDate = $this->endDate;
		$record->displayName = $this->displayName;
		$record->eventName = $this->eventName;
		$record->division = $this->division;
		$record->area = $this->area;
		$record->meta = $this->meta;

		$record->save(false);

		parent::afterSave($isNew);

	}

	/**
	 * @return Subcommittee[]
	 */
	public function getAssignedSubcommittees()
	{
		return CommitteeTools::getInstance()->subcommittees->getSubcommitteesFromNames($this->assignedSubcommitteeNames ?: []);
	}

	/**
	 * @return Subcommittee[]
	 */
	public function getManagedBySubcommittees()
	{
		return CommitteeTools::getInstance()->subcommittees->getSubcommitteesFromNames($this->managedBySubcommitteeNames ?: []);
	}

	/**
	 * @return MemberRecord[]
	 */
	public function getManagingMembers()
	{
		return CommitteeTools::getInstance()->shifts->getManagingMembersForShift($this);
	}

}
