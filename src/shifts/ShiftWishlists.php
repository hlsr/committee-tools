<?php
namespace hlsr\committeetools\shifts;

use Craft;

class ShiftWishlists
{

	public function addShiftToMemberWishlist(int $cn, int $shiftId)
	{
		Craft::$app->db->createCommand()->upsert(
			ShiftWishlistRecord::tableName(),
			[
				'cn' => $cn,
				'shiftId' => $shiftId
			],
			true
		)->execute();
	}

	public function clearMemberWishlist(int $cn)
	{
		ShiftWishlistRecord::deleteAll([
			'cn' => $cn,
		]);
	}

	public function removeShiftFromMemberWishlist(int $cn, int $shiftId)
	{
		ShiftWishlistRecord::deleteAll([
			'cn' => $cn,
			'shiftId' => $shiftId,
		]);
	}

	public function getShiftIdsByMember($member)
	{
		return ShiftWishlistRecord::find()->forMember($member)->shiftIds();
	}

	public function getCnsByShift($shift)
	{
		return ShiftWishlistRecord::find()->forShift($shift)->cns();
	}

}
