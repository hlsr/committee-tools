<?php
namespace hlsr\committeetools\shifts;

use craft\helpers\DateTimeHelper;
use hlsr\committeetools\base\BaseRecord;

/**
 * @property $id
 * @property $uniqueKey
 * @property $showYear
 * @property $assignedSubcommitteeNames
 * @property $managedBySubcommitteeNames
 * @property $startDate
 * @property $endDate
 * @property $displayName
 * @property $eventName
 * @property $division
 * @property $area
 * @property $meta
 */
class ShiftRecord extends BaseRecord
{

	const TableName = 'hlsr_committeetools_shifts';

	protected $dateTimeAttributes = [
		'startDate',
		'endDate',
	];

}
