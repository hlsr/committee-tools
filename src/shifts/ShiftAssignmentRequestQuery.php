<?php
namespace hlsr\committeetools\shifts;

use craft\elements\User;
use craft\helpers\Db;
use yii\db\ActiveQuery;

class ShiftAssignmentRequestQuery extends ActiveQuery
{

	public function type($value): self
	{
		return $this->andWhere(['type' => $value]);
	}

	public function resolvedDate($param): self
	{
		return $this->andWhere(Db::parseDateParam('resolvedDate', $param));
	}

	public function expired(): self
	{
		return $this->andWhere(Db::parseDateParam('expiryDate', time(), '<='));
	}

	/**
	 * @return ShiftAssignmentRequestQuery
	 */
	public function notExpired()
	{
		return $this->andWhere(Db::parseDateParam('expiryDate', time(), '>'));
	}

	/**
	 * @return ShiftAssignmentRequestQuery
	 */
	public function isPending()
	{
		return $this->notExpired()->andWhere(['status' => ShiftAssignmentRequestRecord::STATUS_PENDING]);
	}

	/**
	 * @return ShiftAssignmentRequestQuery
	 */
	public function isApproved()
	{
		return $this->andWhere(['status' => ShiftAssignmentRequestRecord::STATUS_APPROVED]);
	}

	/**
	 * @param ShiftAssignmentRecord|int $assignment
	 *
	 * @return ShiftAssignmentRequestQuery
	 */
	public function involvingShiftAssignment($assignment): self
	{

		if ($assignment instanceof ShiftAssignmentRecord)
		{
			$assignment = $assignment->id;
		}

		return $this
			->andWhere([
				'or',
				['sourceShiftAssignmentId' => $assignment],
				['targetShiftAssignmentId' => $assignment],
			]);

	}

	/**
	 * @param $user
	 *
	 * @return ShiftAssignmentRequestQuery
	 */
	public function involvingUser($user)
	{

		if ($user instanceof User)
		{
			$user = $user->id;
		}

		return $this
			->andWhere([
				'or',
				['initiatingUserId' => $user],
				['resolvingUserId' => $user],
			]);

	}

}
