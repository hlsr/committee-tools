<?php
namespace hlsr\committeetools\shifts;

use Craft;
use craft\helpers\DateTimeHelper;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\members\MemberRecord;
use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidArgumentException;

class ShiftAssignmentRequests extends Component
{

	/**
	 * @var int
	 */
	private $_currentUserId;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
		$this->_currentUserId = Craft::$app->getUser()->getId();
	}

	public function getRequestById(int $id = 0): ?ShiftAssignmentRequestRecord
	{
		return ShiftAssignmentRequestRecord::findOne($id);
	}

	/**
	 * @return ShiftAssignmentRequestQuery
	 */
	public function find()
	{
		return ShiftAssignmentRequestRecord::find();
	}

	/**
	 * @return ShiftAssignmentRequestRecord[]
	 */
	public function getPendingTradeRequestsForAssignment(ShiftAssignmentRecord $assignment): array
	{
		return ShiftAssignmentRequestRecord::find()
			->isPending()
			->involvingShiftAssignment($assignment)
			->type(ShiftAssignmentRequestRecord::TYPE_TRADE)
			->all();
	}

	/**
	 * @return ShiftAssignmentRequestRecord|false
	 *
	 * @throws \Exception
	 */
	public function createTradeRequest(ShiftAssignmentRecord $sourceAssignment, ShiftAssignmentRecord $targetAssignment)
	{

		if ((!$sourceAssignment->availableForTrade) || $sourceAssignment->tradePending)
		{
			throw new InvalidArgumentException("The source Assignment is not marked Available for Trade.");
		}

		if ((!$targetAssignment->availableForTrade) || $targetAssignment->tradePending)
		{
			throw new InvalidArgumentException("The target Assignment is not marked Available for Trade.");
		}

		$transaction = Craft::$app->getDb()->beginTransaction();

		try
		{

			$tradeRequestExpiryDuration = CommitteeTools::getInstance()->getSettings()->tradeRequestExpiryDuration;

			$tradeRequest = new ShiftAssignmentRequestRecord([
				'sourceShiftAssignmentId' => $sourceAssignment->id,
				'targetShiftAssignmentId' => $targetAssignment->id,
				'type' => ShiftAssignmentRequestRecord::TYPE_TRADE,
				'status' => ShiftAssignmentRequestRecord::STATUS_PENDING,
				'expiryDate' => (new \DateTime())->add(DateTimeHelper::secondsToInterval($tradeRequestExpiryDuration)),
				'initiatingUserId' => $this->_currentUserId
			]);

			$success = $tradeRequest->save();

			$sourceAssignment->tradePending = true;
			$success = $success && $sourceAssignment->save();

			$targetAssignment->tradePending = true;
			$success = $success && $targetAssignment->save();

			if ($success)
			{
				$transaction->commit();
			}
			else
			{
				$transaction->rollBack();
			}

		}
		catch (\Exception $e)
		{
			$transaction->rollBack();
			throw $e;
		}

		if ($success)
		{
			// TODO: Put this in a Queue for better performance.
			$this->sendTradeRequestCreatedNotifications($tradeRequest);
		}

		return $success ? $tradeRequest : false;

	}

	public function resolveRequestWithStatus(ShiftAssignmentRequestRecord $request, string $status, bool $refreshPendingRequestsStatus = true): bool
	{

		try
		{

			if ($request->isResolved()) {
				// TODO: What should we actually return here?
				throw new Exception("The Shift Assignment Request is already Resolved.");
			}

			$request->status = $status;
			$request->resolvedDate = new \DateTime();
			$request->resolvingUserId = Craft::$app->getUser()->getId();

			$success = $request->save();

		}
		catch(\Exception $e)
		{
			$success = false;
		}

		if ($refreshPendingRequestsStatus)
		{
			if ($sourceAssignment = $request->getSourceAssignment())
			{
				CommitteeTools::getInstance()->assignments->refreshPendingRequestsStatus($sourceAssignment);
			}
			if ($targetAssignment = $request->getTargetAssignment())
			{
				CommitteeTools::getInstance()->assignments->refreshPendingRequestsStatus($targetAssignment);
			}
		}

		return $success;

	}

	/**
	 * @throws \Exception
	 */
	public function approveRequest(ShiftAssignmentRequestRecord $request): bool
	{

		// TODO: Make sure the request isn't Expired

		if ($request->type == ShiftAssignmentRequestRecord::TYPE_TRADE)
		{
			return $this->_approveTradeRequest($request);
		}

		// TODO: Logic for approving Pickup requests

		return false;

	}

	public function cancelRequest(ShiftAssignmentRequestRecord $request): bool
	{
		return $this->resolveRequestWithStatus($request, ShiftAssignmentRequestRecord::STATUS_CANCELLED);
		// TODO: Event handler
	}

	public function declineRequest(ShiftAssignmentRequestRecord $request): bool
	{

		if ($request->type == ShiftAssignmentRequestRecord::TYPE_TRADE)
		{
			return $this->_declineTradeRequest($request);
		}

		// TODO: Logic for approving Pickup requests
		return $this->resolveRequestWithStatus($request, ShiftAssignmentRequestRecord::STATUS_DECLINED);

		// TODO: Event handler

	}

	public function voidRequest(ShiftAssignmentRequestRecord $request): bool
	{
		return $this->resolveRequestWithStatus($request, ShiftAssignmentRequestRecord::STATUS_VOIDED);
		// TODO: Event handler
	}

	/**
	 * @throws \Exception
	 */
	private function _approveTradeRequest(ShiftAssignmentRequestRecord $request): bool
	{

		$transaction = Craft::$app->getDb()->beginTransaction();

		try
		{

			// Ensure we have both Assignments handy

			$sourceAssignment = $request->getSourceAssignment();
			if (!$sourceAssignment)
			{
				throw new Exception("Source Assignment must be defined.");
			}

			$targetAssignment = $request->getTargetAssignment();
			if (!$targetAssignment)
			{
				throw new Exception("Target Assignment must be defined.");
			}

			// Grab the member CNs before we update them.

			$sourceMemberCn = $sourceAssignment->assignedMemberCn;
			$targetMemberCn = $targetAssignment->assignedMemberCn;

			// Reassign and refresh the source Assignment

			$sourceAssignment->availableForTrade = false;
			$sourceAssignment->tradePending = false;
			$sourceAssignment->tradeCompleted = true;
			$sourceAssignment->assignedMemberCn = $targetMemberCn;

			$success = $sourceAssignment->save();

			// Reassign and refresh the target Assignment

			$targetAssignment->availableForTrade = false;
			$targetAssignment->tradePending = false;
			$targetAssignment->tradeCompleted = true;
			$targetAssignment->assignedMemberCn = $sourceMemberCn;

			$success = $success && $targetAssignment->save();

			// Resolve the Request

			$success = $success && $this->resolveRequestWithStatus($request, ShiftAssignmentRequestRecord::STATUS_APPROVED, false);

			// TODO: Void any other outstanding Trade Requests

			if ($success)
			{
				$transaction->commit();
			}
			else
			{
				$transaction->rollBack();
			}

			if ($success)
			{
				// TODO: Put this in a Queue for better performance.
				$this->sendTradeRequestAcceptedNotifications($request);
			}

		}
		catch (\Exception $e)
		{
			$transaction->rollBack();
			throw $e;
		}

		return $success;

		// TODO: Event handlers

	}

	private function _declineTradeRequest(ShiftAssignmentRequestRecord $request): bool
	{

		$success = $this->resolveRequestWithStatus($request, ShiftAssignmentRequestRecord::STATUS_DECLINED);

		if ($success)
		{
			// TODO: Put this in a Queue for better performance.
			$this->sendTradeRequestDeclinedNotifications($request);
		}

		return $success;

		// TODO: Event handlers

	}

	/**
	 * @return bool
	 */
	public function processExpiredRequests()
	{

		$unprocessedExpiredRequests = ShiftAssignmentRequestRecord::find()
			->expired()
			->andWhere(['status' => ShiftAssignmentRequestRecord::STATUS_PENDING])
			->all();

		$success = true;

		foreach ($unprocessedExpiredRequests as $request)
		{
			$success = $this->resolveRequestWithStatus($request, ShiftAssignmentRequestRecord::STATUS_EXPIRED) && $success;
		}

		return $success;

	}

	public function sendTradeRequestCreatedNotifications(ShiftAssignmentRequestRecord $request)
	{
		CommitteeTools::getInstance()->notifications->sendEmailNotification(
			"Trade Request Submitted",
			$request->getSourceAssignment()->assignedMember->email,
			'_notifications/tradeRequestSubmitted-toRequestor',
			['tradeRequest' => $request]
		);

		CommitteeTools::getInstance()->notifications->sendEmailNotification(
			"New Shift Trade Request from " . $request->getSourceAssignment()->assignedMember->getFullPreferredName(),
			$request->getTargetAssignment()->assignedMember->email,
			'_notifications/tradeRequestSubmitted-toRequestee',
			['tradeRequest' => $request]
		);
	}

	public function sendTradeRequestAcceptedNotifications(ShiftAssignmentRequestRecord $request)
	{

		// To requestor:

		CommitteeTools::getInstance()->notifications->sendEmailNotification(
			"Shift Trade Request accepted by " . $request->getTargetAssignment()->assignedMember->getFullPreferredName(),
			$request->getSourceAssignment()->assignedMember->email,
			'_notifications/tradeRequestAccepted-toRequestor',
			['tradeRequest' => $request]
		);

		// To managers of source Shift:

		$sourceShiftManagers = CommitteeTools::getInstance()->shifts->getManagingMembersForShift($request->getSourceAssignment()->shift);

		if (!empty($sourceShiftManagers))
		{

			$sourceShiftManagerEmails = array_map(
				function (MemberRecord $member)
				{
					return $member->email;
				},
				$sourceShiftManagers
			);

			CommitteeTools::getInstance()->notifications->sendEmailNotification(
				"Trade Request notification: " . $request->getSourceAssignment()->shift->displayName,
				$sourceShiftManagerEmails,
				'_notifications/tradeRequestAccepted-toManagers',
				['tradeRequest' => $request]
			);

		}


		// To managers of Target Shift:

		$targetShiftManagers = CommitteeTools::getInstance()->shifts->getManagingMembersForShift($request->getTargetAssignment()->shift);

		if (!empty($targetShiftManagers))
		{

			$targetShiftManagerEmails = array_map(
				function (MemberRecord $member)
				{
					return $member->email;
				},
				$targetShiftManagers
			);

			CommitteeTools::getInstance()->notifications->sendEmailNotification(
				"Trade Request notification: " . $request->getTargetAssignment()->shift->displayName,
				$targetShiftManagerEmails,
				'_notifications/tradeRequestAccepted-toManagers',
				['tradeRequest' => $request]
			);

		}


	}

	public function sendTradeRequestDeclinedNotifications(ShiftAssignmentRequestRecord $request)
	{
		CommitteeTools::getInstance()->notifications->sendEmailNotification(
			"Trade Request declined by " . $request->getTargetAssignment()->assignedMember->getFullPreferredName(),
			$request->getSourceAssignment()->assignedMember->email,
			'_notifications/tradeRequestDeclined-toRequestor',
			['tradeRequest' => $request]
		);
	}

}
