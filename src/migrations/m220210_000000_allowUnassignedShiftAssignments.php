<?php
namespace hlsr\committeetools\migrations;

use craft\db\Migration;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;

class m220210_000000_allowUnassignedShiftAssignments extends Migration
{

	public function safeUp() : bool
	{

		$this->dropForeignKey('hlsr_committeetools_shift_assignments_assignedMemberCn_fk', ShiftAssignmentRecord::tableName());

		$this->alterColumn(ShiftAssignmentRecord::tableName(), 'assignedMemberCn', $this->integer()->unsigned()->null());

		$this->addForeignKey(
			'hlsr_committeetools_shift_assignments_assignedMemberCn_fk',
			ShiftAssignmentRecord::tableName(),
			['assignedMemberCn'],
			MemberRecord::tableName(),
			['cn'],
			'SET NULL'
		);

		return true;

	}

	public function safeDown() : bool
	{

		$this->dropForeignKey('hlsr_committeetools_shift_assignments_assignedMemberCn_fk', ShiftAssignmentRecord::tableName());

		$this->alterColumn(ShiftAssignmentRecord::tableName(), 'assignedMemberCn', $this->integer()->unsigned()->notNull());

		$this->addForeignKey(
			'hlsr_committeetools_shift_assignments_assignedMemberCn_fk',
			ShiftAssignmentRecord::tableName(),
			['assignedMemberCn'],
			MemberRecord::tableName(),
			['cn'],
			'CASCADE'
		);

		return true;

	}

}
