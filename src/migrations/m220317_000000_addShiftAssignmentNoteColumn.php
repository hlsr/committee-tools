<?php
namespace hlsr\committeetools\migrations;

use craft\db\Migration;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;

class m220317_000000_addShiftAssignmentNoteColumn extends Migration
{

	/**
	 * @inheritdoc
	 */
	public function safeUp() : bool
	{
		$this->addColumn(
			ShiftAssignmentRecord::tableName(),
			'note',
			$this->text()->after('checkedOutByUserId')
		);
		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() : bool
	{
		$this->dropColumn(
			ShiftAssignmentRecord::tableName(),
			'note'
		);
		return true;
	}

}
