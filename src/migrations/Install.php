<?php
namespace hlsr\committeetools\migrations;

use craft\db\Migration;
use craft\db\Table;
use hlsr\committeetools\checkins\CheckinRecord;
use hlsr\committeetools\members\CommitteeServiceHistoryRecord;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;
use hlsr\committeetools\shifts\ShiftAssignmentRequestRecord;
use hlsr\committeetools\shifts\ShiftAssignmentStatusUpdateRecord;
use hlsr\committeetools\shifts\ShiftRecord;

/**
 * @package hlsr\committeetools\migrations
 */
class Install extends Migration
{

	/**
	 * @inheritdoc
	 */
	public function safeUp() : bool
	{
		return $this->_addTables();
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() : bool
	{
		return $this->_removeTables();
	}


	private function _addTables()
	{

		/*
		 * Add Members table
		 */

		if (!$this->db->tableExists(MemberRecord::tableName())) {

			$this->createTable(MemberRecord::tableName(), [

				'cn' => $this->integer()->unsigned()->notNull(),

				'subcommitteeName1' => $this->string(),
				'subcommitteeName2' => $this->string(),
				'subcommitteeName3' => $this->string(),

				'description' => $this->string(),
				'customerName' => $this->string(),
				'customerNamePhonetic' => $this->string(),
				'firstName' => $this->string(),
				'lastName' => $this->string(),
				'preferredName' => $this->string(),
				'legalNameVerified' => $this->string(),
				'address' => $this->string(),
				'city' => $this->string(45), // Chargoggagoggmanchauggagoggchaubunagungamaugg
				'state' => $this->string(52), // The State of Rhode Island and Providence Plantations
				'zip' => $this->string(10), // TIL: Iran has 10-digit postal codes.
				'phone' => $this->string(42),
				'cellPhone' => $this->string(42),
				'workPhone' => $this->string(42),
				'email' => $this->string(),
				'showDues' => $this->boolean(),
				'commDues' => $this->boolean(),
				'indemnityInd' => $this->boolean(),
				'harassmentInd' => $this->boolean(),
				'badgeReleased' => $this->boolean(),
				'badgeReleaseDate' => $this->dateTime(),
				'rookie' => $this->boolean(),
				'badgeIssueDate' => $this->dateTime(),
				'badgePickupPerson' => $this->string(),
				'ltcApplied' => $this->boolean(),
				'inOtherCommittees' => $this->boolean(),

				'photoUrl' => $this->string(), // =Hyperlink("https://secure.rodeohouston.com/MembershipChairTools/MSHPExternalImagePipe.aspx?Type=LightRoster&CN=1188272&CI=8052")

				'lightRosterTitle' => $this->string(),
				'lightRosterCustomerName' => $this->string(),
				'lightRosterCustomerNamePhonetic' => $this->string(),
				'lightRosterFirstName' => $this->string(),
				'lightRosterLastName' => $this->string(),
				'lightRosterCommitteeName' => $this->string(),
				'lightRosterAddress' => $this->string(),
				'lightRosterCityStateZip' => $this->string(),
				'lightRosterHomePhone' => $this->string(42),
				'lightRosterCellPhone' => $this->string(42),
				'lightRosterWorkPhone' => $this->string(42),
				'lightRosterEmail' => $this->string(),
				'lightRosterSpouse' => $this->string(),
				'lightRosterEmployer' => $this->string(),
				'lightRosterJobTitle' => $this->string(),

				'emergencyContactName' => $this->string(),
				'emergencyContactRelationship' => $this->string(),
				'emergencyContactTelephone' => $this->string(42),
				'emergencyContactEmail' => $this->string(),

				'status' => $this->string(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

				'PRIMARY KEY(cn)',

			]);

		}

		/*
		 * Add Committee Service History table
		 */

		if (!$this->db->tableExists(CommitteeServiceHistoryRecord::tableName())) {

			$this->createTable(CommitteeServiceHistoryRecord::tableName(), [

				'cn' => $this->integer()->unsigned()->notNull(),

				'showYear' => $this->integer()->unsigned()->notNull(),
				'customerName' => $this->string(),
				'committeeName' => $this->string(),
				'titleName' => $this->string(),
				'startDateActive' => $this->dateTime(),
				'activeVolunteer' => $this->boolean(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

			]);

			$this->addPrimaryKey(null, CommitteeServiceHistoryRecord::tableName(), ['cn', 'showYear']);

		}

		/*
		 * Add Shifts table
		 */

		if (!$this->db->tableExists(ShiftRecord::tableName())) {

			$this->createTable(ShiftRecord::tableName(), [

				'id' => $this->integer()->notNull(),
				'uniqueKey' => $this->string(),

				'showYear' => $this->integer()->unsigned()->notNull(),
				'assignedSubcommitteeNames' => $this->string(),
				'managedBySubcommitteeNames' => $this->string(),
				'startDate' => $this->dateTime(),
				'endDate' => $this->dateTime(),
				'displayName' => $this->string(),
				'eventName' => $this->string(),
				'division' => $this->string(),
				'area' => $this->string(),

				'meta' => $this->mediumText(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

				'PRIMARY KEY(id)',

			]);

			$this->createIndex(null, ShiftRecord::tableName(), ['uniqueKey'], true);

			$this->addForeignKey(
				null,
				ShiftRecord::tableName(),
				['id'],
				Table::ELEMENTS,
				['id'],
				'CASCADE'
			);

		}

		/*
		 * Add Shift Assignments table
		 */

		// TODO: Clean up this schema with default values and not-nulls for the boolean flags?

		if (!$this->db->tableExists(ShiftAssignmentRecord::tableName())) {

			$this->createTable(ShiftAssignmentRecord::tableName(), [

				'id' => $this->primaryKey(),
				'shiftId' => $this->integer()->notNull(),
				'assignedMemberCn' => $this->integer()->unsigned()->notNull(),

				'originalAssignedMemberCn' => $this->integer()->unsigned(),
				'type' => $this->string(),
				'area' => $this->string(),
				'position' => $this->string(),
				'status' => $this->string(),
				'availableForTrade' => $this->boolean(),
				'tradePending' => $this->boolean(),
				'tradeCompleted' => $this->boolean(),
				'availableForPickup' => $this->boolean(),
				'pickupPending' => $this->boolean(),
				'pickupCompleted' => $this->boolean(),
				'locked' => $this->boolean(),
				'checkedInTime' => $this->dateTime(),
				'checkedInByUserId' => $this->integer(),
				'checkedOutTime' => $this->dateTime(),
				'checkedOutByUserId' => $this->integer(),

				'meta' => $this->mediumText(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

			]);

			$this->addForeignKey(
				'hlsr_committeetools_shift_assignments_shiftId_fk',
				ShiftAssignmentRecord::tableName(),
				['shiftId'],
				ShiftRecord::tableName(),
				['id'],
				'CASCADE'
			);

			$this->addForeignKey(
				'hlsr_committeetools_shift_assignments_assignedMemberCn_fk',
				ShiftAssignmentRecord::tableName(),
				['assignedMemberCn'],
				MemberRecord::tableName(),
				['cn'],
				'CASCADE'
			);

		}

		/*
		 * Add Shift Assignment Status Updates table
		 */

		if (!$this->db->tableExists(ShiftAssignmentStatusUpdateRecord::tableName())) {

			$this->createTable(ShiftAssignmentStatusUpdateRecord::tableName(), [

				'id' => $this->primaryKey(),
				'shiftAssignmentId' => $this->integer()->notNull(),

				'status' => $this->string()->notNull(),
				'updatedByUserId' => $this->integer(),
				'updatedFromLocation' => $this->string(),
				'comments' => $this->string(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

			]);

			$this->addForeignKey(
				null,
				ShiftAssignmentStatusUpdateRecord::tableName(),
				['shiftAssignmentId'],
				ShiftAssignmentRecord::tableName(),
				['id'],
				'CASCADE'
			);

		}

		/*
		 * Add Shift Assignment Requests table
		 */

		if (!$this->db->tableExists(ShiftAssignmentRequestRecord::tableName())) {

			$this->createTable(ShiftAssignmentRequestRecord::tableName(), [

				'id' => $this->primaryKey(),

				'sourceShiftAssignmentId' => $this->integer(),
				'targetShiftAssignmentId' => $this->integer()->notNull(),
				'type' => $this->string(),
				'status' => $this->string(),
				'expiryDate' => $this->dateTime(),
				'resolvedDate' => $this->dateTime(),
				'initiatingUserId' => $this->integer(),
				'resolvingUserId' => $this->integer(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

			]);

		}

		/*
		 * Add Checkins table
		 */

		if (!$this->db->tableExists(CheckinRecord::tableName())) {

			$this->createTable(CheckinRecord::tableName(), [

				'id' => $this->primaryKey(),

				'showYear' => $this->integer()->unsigned()->notNull(),
				'cn' => $this->integer()->unsigned()->notNull(),
				'location' => $this->string(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

			]);

		}

		return true;

	}

	private function _removeTables()
	{

		// Drop tables in reverse of the order we created them, to avoid foreign key constraint failures.

		$this->dropTableIfExists(CheckinRecord::tableName());
		$this->dropTableIfExists(ShiftAssignmentRequestRecord::tableName());
		$this->dropTableIfExists(ShiftAssignmentStatusUpdateRecord::tableName());
		$this->dropTableIfExists(ShiftAssignmentRecord::tableName());
		$this->dropTableIfExists(ShiftRecord::tableName());
		$this->dropTableIfExists(CommitteeServiceHistoryRecord::tableName());
		$this->dropTableIfExists(MemberRecord::tableName());

		return true;

	}

}
