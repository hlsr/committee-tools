<?php
namespace hlsr\committeetools\migrations;

use craft\db\Migration;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\shifts\ShiftRecord;
use hlsr\committeetools\shifts\ShiftWishlistRecord;

class m220113_000000_installWishlistTable extends Migration
{

	/**
	 * @inheritdoc
	 */
	public function safeUp() : bool
	{
		return $this->_addTables();
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() : bool
	{
		return $this->_removeTables();
	}


	private function _addTables()
	{

		/*
		 * Add Wishlist table
		 */

		if (!$this->db->tableExists(ShiftWishlistRecord::tableName())) {

			$this->createTable(ShiftWishlistRecord::tableName(), [

				'cn' => $this->integer()->unsigned()->notNull(),
				'shiftId' => $this->integer()->notNull(),

				'dateCreated' => $this->dateTime()->notNull(),
				'dateUpdated' => $this->dateTime()->notNull(),
				'uid' => $this->uid(),

				'PRIMARY KEY(cn, shiftId)',

			]);

			$this->addForeignKey(
				null,
				ShiftWishlistRecord::tableName(),
				['cn'],
				MemberRecord::tableName(),
				['cn'],
				'CASCADE'
			);

			$this->addForeignKey(
				null,
				ShiftWishlistRecord::tableName(),
				['shiftId'],
				ShiftRecord::tableName(),
				['id'],
				'CASCADE'
			);

		}

		return true;

	}

	private function _removeTables()
	{

		$this->dropTableIfExists(ShiftWishlistRecord::tableName());

		return true;

	}

}
