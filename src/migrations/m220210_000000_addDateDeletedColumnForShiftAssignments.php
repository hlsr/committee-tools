<?php
namespace hlsr\committeetools\migrations;

use craft\db\Migration;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;

class m220210_000000_addDateDeletedColumnForShiftAssignments extends Migration
{

	/**
	 * @inheritdoc
	 */
	public function safeUp() : bool
	{
		try
		{
			$this->addColumn(ShiftAssignmentRecord::tableName(), 'dateDeleted', $this->dateTime()->null()->after('dateUpdated'));
			return true;
		}
		catch (\Exception $e)
		{
			return false;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() : bool
	{
		try
		{
			$this->dropColumn(ShiftAssignmentRecord::tableName(), 'dateDeleted');
			return true;
		}
		catch (\Exception $e)
		{
			return false;
		}
	}

}
