<?php
namespace hlsr\committeetools\assetbundles;

use craft\web\AssetBundle;

class MemberSearchAssetBundle extends AssetBundle
{

	public function init()
	{

		$this->sourcePath = '@hlsr/committeetools/assetbundles';

		$this->depends = [
			AlpineAssetBundle::class
		];

		$this->js = [
			'js/lib/algoliasearch-lite.umd.js',
			'js/debounce.js',
			'js/MemberSearch.js',
		];

		$this->css = [
		];

		parent::init();

	}

}
