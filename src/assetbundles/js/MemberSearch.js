const client = algoliasearch(algolia.appId, algolia.searchKey);
const index = client.initIndex(committeeTools.membersIndexName);

function x_memberSearch() {

	return {

		query: '',

		loading: true,

		hits: [],

		timeout: null,

		search: debounce(function() {

			this.loading = true;

			index.search(this.query, {
				attributesToRetrieve: [
					'cn',
					'fullPreferredName',
					'description',
					'email',
					'bestPhoneNumber',
					'phone',
					'cellPhone',
					'subcommittee1DisplayName',
					'lightRosterEmail',
					'lightRosterPhone',
					'lightRosterCellPhone',
					'photoUrl'
				],
				attributesToHighlight: [
					'fullPreferredName',
					'email',
					'lightRosterEmail',
					'description'
				],
				highlightPreTag: "<span class='searchHighlight'>",
				highlightPostTag: "</span>",
				hitsPerPage: 60,
				facetFilters: [
					'status:active'
				]
			}).then(({ hits }) => {
				this.loading = false;
				this.hits = hits;
				// console.log("Searched: " + this.query, this.hits);
			});

		}, 200)

	};

}
