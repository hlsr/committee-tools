<?php
namespace hlsr\committeetools\assetbundles;

use craft\web\AssetBundle;

class AlpineAssetBundle extends AssetBundle
{

	public function init()
	{

		$this->sourcePath = '@hlsr/committeetools/assetbundles';

		$this->depends = [
			// CpAsset::class,
		];

		$this->js = [
			'js/lib/alpine-ie11.js',
		];

		$this->css = [
		];

		parent::init();

	}

}
