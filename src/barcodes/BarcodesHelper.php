<?php
namespace hlsr\committeetools\barcodes;

use Picqer\Barcode\BarcodeGeneratorHTML;
use Picqer\Barcode\BarcodeGeneratorSVG;

class BarcodesHelper
{

	public static function getBarcodeSvg(string $content, string $type, $widthFactor = null, $totalHeight = null): string
	{
		$generator = new BarcodeGeneratorSVG();
		return $generator->getBarcode($content, $type, $widthFactor, $totalHeight);
	}

	public static function getBarcodeHtml(string $content, string $type): string
	{
		$generator = new BarcodeGeneratorHTML();
		return $generator->getBarcode($content, $type);
	}

}
