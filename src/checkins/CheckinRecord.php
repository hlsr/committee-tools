<?php
namespace hlsr\committeetools\checkins;

use hlsr\committeetools\base\BaseRecord;

/**
 * @property int $id
 * @property int $showYear
 * @property int $cn
 * @property string $location
 */
class CheckinRecord extends BaseRecord
{

	const TableName = 'hlsr_committeetools_checkins';

}
