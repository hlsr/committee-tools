<?php
namespace hlsr\committeetools\checkins;

use craft\helpers\DateTimeHelper;
use DateTime;
use hlsr\committeetools\CommitteeTools;
use yii\base\Component;

class Checkins extends Component
{

	public function checkInMember(int $cn, string $location = null, ?DateTime $date = null): bool
	{

		$member = CommitteeTools::getInstance()->members->getMemberByCn($cn);

		// Find the Member.

		if (!$member)
		{
			return false;
		}

		// Record the check-in.

		$checkin = new CheckinRecord([
			'cn' => $member->cn,
			'showYear' => CommitteeTools::getInstance()->getSettings()->showYear,
			'location' => $location
		]);
		$checkin->save();

		// Attempt to check in the Member's Assignments on the given date.

		try
		{

			$assignments = CommitteeTools::getInstance()->assignments->find()
				->assignedTo((int)$cn)
				->onDay($date ?: time())
				->all();

			foreach ($assignments as $assignment)
			{
				CommitteeTools::getInstance()->assignments->checkInAssignment($assignment, $location);
			}

		}
		catch (\Exception $e)
		{
			CommitteeTools::error($e->getMessage());
			return false;
		}

		return true;

	}

	public function replayCheckin(CheckinRecord $checkin): bool
	{
		return $this->checkInMember($checkin->cn, $checkin->location, $checkin->dateCreated);
	}

}
