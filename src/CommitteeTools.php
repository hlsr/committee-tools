<?php
namespace hlsr\committeetools;

use Craft;
use craft\base\Plugin;
use craft\console\Application as ConsoleApplication;
use craft\elements\Entry;
use craft\events\ModelEvent;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\helpers\FileHelper;
use craft\services\Elements;
use craft\services\Fields;
use craft\web\Application as WebApplication;
use craft\web\UrlManager;
use craft\web\View as CraftView;
use hlsr\committeetools\checkins\Checkins;
use hlsr\committeetools\config\Settings;
use hlsr\committeetools\members\MemberRecord;
use hlsr\committeetools\members\Members;
use hlsr\committeetools\notifications\Notifications;
use hlsr\committeetools\rosters\Rosters;
use hlsr\committeetools\rosters\Subcommittees;
use hlsr\committeetools\search\Search;
use hlsr\committeetools\shifts\ShiftAssignmentRequests;
use hlsr\committeetools\shifts\ShiftAssignments;
use hlsr\committeetools\shifts\ShiftElement;
use hlsr\committeetools\shifts\Shifts;
use hlsr\committeetools\shifts\ShiftsField;
use hlsr\committeetools\shifts\ShiftWishlists;
use hlsr\committeetools\view\TemplateHooks;
use hlsr\committeetools\view\TwigExtension;
use hlsr\committeetools\view\View;
use hlsr\committeetools\web\Router;
use yii\base\Event;

/**
 * Module to encapsulate Committee Tools functionality.
 *
 * This class will be available throughout the system via:
 * `Craft::$app->getModule('committee-tools')`
 *
 * @see http://www.yiiframework.com/doc-2.0/guide-structure-modules.html
 *
 * @property ShiftAssignments $assignments
 * @property ShiftAssignmentRequests $assignmentRequests
 * @property Checkins $checkins
 * @property Notifications $notifications
 * @property Members $members
 * @property Rosters $rosters
 * @property Router $router
 * @property Search $search
 * @property Shifts $shifts
 * @property Subcommittees $subcommittees
 * @property View $view
 * @property ShiftWishlists $wishlists
 *
 * @method Settings getSettings()
 */
class CommitteeTools extends Plugin
{

	/**
	 * @var bool
	 */
	public $hasCpSection = false;

	/**
	 * @var bool
	 */
	public $hasCpSettings = false;

	/**
	 * @var string
	 */
	public $schemaVersion = '1.2022.28.1';

	/**
	 * @var MemberRecord
	 */
	private $_currentMember;
	
	/*
	 * Methods
	 */

	public function __construct($id, $parent = null, array $config = [])
	{

		$config['components'] = [

			'assignments' => ShiftAssignments::class,
			'assignmentRequests' => ShiftAssignmentRequests::class,
			'checkins' => Checkins::class,
			'members' => Members::class,
			'notifications' => Notifications::class,
			'rosters' => Rosters::class,
			'router' => Router::class,
			'search' => Search::class,
			'shifts' => Shifts::class,
			'subcommittees' => Subcommittees::class,
			'view' => View::class,
			'wishlists' => ShiftWishlists::class,

		];

		parent::__construct($id, $parent, $config);

	}

	/**
	 * Initializes the module.
	 */
	public function init()
	{

		parent::init();

		// Register components

		$this->_registerElementTypes();
		$this->_registerFieldTypes();

		// Routing and controller namespace

		$this->_registerSiteUrlRules();

		if (Craft::$app instanceof ConsoleApplication)
		{
			$this->controllerNamespace = 'hlsr\\committeetools\\controllers\\console';
		}
		if (Craft::$app instanceof WebApplication)
		{
			$this->controllerNamespace = 'hlsr\\committeetools\\controllers\\web';
		}

		// Add custom View functionality

		$this->_registerTemplateRoots();
		Craft::$app->view->registerTwigExtension(new TwigExtension());
		Craft::$app->view->hook('cp.users.edit.details', [TemplateHooks::class, 'cpUsersEditDetails']);

		// Register event handlers

		Event::on(
			Entry::class,
			Entry::EVENT_AFTER_PROPAGATE,
			function (ModelEvent $event)
			{
				if (is_callable($this->getSettings()->entryAfterPropagateHandler))
				{
					($this->getSettings()->entryAfterPropagateHandler)($event->sender);
				}

			}
		);

	}

	public function getCurrentMember(): ?MemberRecord
	{
		if (!isset($this->_currentMember))
		{
			$currentUser = Craft::$app->getUser()->getIdentity();
			$this->_currentMember = CommitteeTools::getInstance()->members->getUserMember($currentUser);
		}
		return $this->_currentMember;
	}

	/**
	 * Creates and returns the model used to store the plugin’s settings.
	 *
	 * @return Settings|null
	 */
	protected function createSettingsModel()
	{
		return new Settings();
	}

	/**
	 * Register the element types supplied by the plugin.
	 */
	private function _registerElementTypes()
	{
		Event::on(Elements::class, Elements::EVENT_REGISTER_ELEMENT_TYPES, function(RegisterComponentTypesEvent $e) {
			$e->types[] = ShiftElement::class;
		});
	}

	private function _registerFieldTypes()
	{
		Event::on(
			Fields::class,
			Fields::EVENT_REGISTER_FIELD_TYPES,
			function (RegisterComponentTypesEvent $event) {
				$event->types[] = ShiftsField::class;
			}
		);
	}

	private function _registerSiteUrlRules()
	{

		Event::on(
			UrlManager::class,
			UrlManager::EVENT_REGISTER_SITE_URL_RULES,
			function (RegisterUrlRulesEvent $event) {
				$event->rules = $event->rules + $this->router->getSystemUrlRules();
			}
		);

	}

	private function _registerTemplateRoots()
	{

		Event::on(
			CraftView::class,
			CraftView::EVENT_REGISTER_SITE_TEMPLATE_ROOTS,
			function(RegisterTemplateRootsEvent $event) {
				$event->roots['__ct'] = __DIR__ . '/templates';
			}
		);

	}
	
	/*
	 * Statics
	 */

	public static function log($msg, string $level = 'notice', string $file = 'CommitteeTools')
	{
		try
		{
			$file = Craft::getAlias('@storage/logs/' . $file . '.log');
			$log = "\n" . date('Y-m-d H:i:s') . " [{$level}]" . "\n" . print_r($msg, true);
			FileHelper::writeToFile($file, $log, ['append' => true]);
		}
		catch(\Exception $e)
		{
			Craft::error($e->getMessage());
		}
	}

	public static function error($msg)
	{
		static::log($msg, 'error');
	}

}
