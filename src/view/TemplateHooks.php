<?php
namespace hlsr\committeetools\view;

use Craft;
use hlsr\committeetools\CommitteeTools;
use yii\helpers\Inflector;

class TemplateHooks
{

	/**
	 * @param int|MemberRecord $member
	 *
	 * @return int|null
	 */
	public static function cpUsersEditDetails(&$context)
	{

		$user = $context['user'];
		/** @var User $user */

		$member = CommitteeTools::getInstance()->members->getUserMember($user);

		if (!$member)
		{
			return '';
		}

		$ret = "<div style=\"padding: 1em;\">
					<h5 style=\"margin-bottom: 1em; font-weight: bolder;\">HLSR Roster Data:</h5>";

		foreach ($member->getAttributes() as $k => $v)
		{

			$k = Inflector::camel2words($k);

			if ($v === 1 || $v === '1')
			{
				$v = '✅';
			}
			if ($v === 0 || $v === '0')
			{
				$v = '👎';
			}
			if ($v instanceof \DateTime)
			{
				$v = $v->format('Y-m-d h:i:s');
			}
			if (is_array($v))
			{
				continue;
			}
			if (empty($v))
			{
				continue;
			}
			$ret .= "<div class=\"data\" style=\"margin-bottom: 1em;\">
						<h5 class=\"heading\">{$k}</h5>
						<p class=\"value\">{$v}</p>
						</div>";
		}

		$ret .= "</div>";

		return $ret;

	}



}
