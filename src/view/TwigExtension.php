<?php
namespace hlsr\committeetools\view;

use Twig\TwigFunction;
use hlsr\committeetools\barcodes\BarcodesHelper;
use hlsr\committeetools\CommitteeTools;

class TwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{

	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'Committee Tools';
	}


	/**
	 * Returns an array of Twig filters, to be used in Twig templates via:
	 *
	 *      {{ 'bar' | fooFilter }}
	 *
	 * @return array
	 */
	public function getFilters()
	{
		return [];
	}


	/**
	 * Returns an array of Twig functions, used in Twig templates via:
	 *
	 *      {% set fizz = fooFunction('buzz') %}
	 *
	 * @return array
	 */
	public function getFunctions()
	{
		return [
			new TwigFunction('ctUrl', [CommitteeTools::getInstance()->router, 'getUrl']),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getGlobals()
	{
		return [
			'ct' => CommitteeTools::getInstance(),
			'committeeTools' => CommitteeTools::getInstance(),
			'barcodes' => new BarcodesHelper(),
			'currentMember' => CommitteeTools::getInstance()->getCurrentMember(),
		];
	}

}
