<?php
namespace hlsr\committeetools\view;

use craft\helpers\Template;
use hlsr\committeetools\CommitteeTools;
use yii\base\Component;

class View extends Component
{

	/**
	 * @param $template
	 * @param array $variables
	 *
	 * @return \Twig\Markup
	 *
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 * @throws \yii\base\Exception
	 *
	 * @deprecated
	 */
	public function render($template, $variables = [])
	{
		return Template::raw(TemplateHelper::renderSystemTemplate($template, $variables));
	}

	public function getSystemTemplatePaths($template): array
	{

		$settings = CommitteeTools::getInstance()->getSettings();

		return [
			$settings->customTemplatePrefix . $template,
			$settings->systemTemplateRoot . '/' . $template,
			$template,
		];

	}

}
