<?php
namespace hlsr\committeetools\view;

use Craft;
use hlsr\committeetools\CommitteeTools;

class TemplateHelper
{

	/**
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 * @throws \yii\base\Exception
	 *
	 * @deprecated
	 */
	public static function renderSystemTemplate(string $template, array $variables = []): string
	{

		$view = Craft::$app->getView();

		foreach (CommitteeTools::getInstance()->view->getSystemTemplatePaths($template) as $path)
		{
			if ($view->doesTemplateExist($path))
			{
				return $view->renderTemplate($path, $variables);
			}

		}

	}

}




