<?php
namespace hlsr\committeetools\members;

use Craft;
use craft\db\Table;
use craft\elements\User;
use craft\helpers\FileHelper;
use craft\helpers\StringHelper;
use GuzzleHttp\Client;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\rosters\Subcommittee;
use yii\base\Component;

class Members extends Component
{

	/**
	 * @param int $cn
	 *
	 * @return MemberRecord|null
	 */
	public function getMemberByCn($cn = 0)
	{
		return MemberRecord::findOne(['cn' => $cn]);
	}

	/**
	 * @param User $user
	 *
	 * @return MemberRecord|null
	 */
	public function getUserMember(?User $user)
	{
		if (empty($user))
		{
			return null;
		}
		return MemberRecord::findOne($user->username);
	}

	/**
	 * @param MemberRecord $member
	 *
	 * @return User|null
	 */
	public function getMemberUser(?MemberRecord $member)
	{
		if (empty($member))
		{
			return null;
		}
		return Craft::$app->getUsers()->getUserByUsernameOrEmail($member->cn);
	}

	/**
	 * @return MemberQuery
	 */
	public function find()
	{
		return MemberRecord::find();
	}

	/**
	 * @param Subcommittee|string $subcommittee
	 *
	 * @return MemberRecord[]
	 */
	public function getMembersBySubcommittee($subcommittee, ?string $orderBy = 'lastName'): array
	{

		$query = MemberRecord::find()->isActive()->onSubcommittee($subcommittee);

		if (!empty($orderBy))
		{
			$query = $query->orderBy($orderBy);
		}

		return $query->all();

	}

	/**
	 * @param Subcommittee|string $subcommittee
	 *
	 * @return MemberRecord[]
	 */
	public function getRookiesBySubcommittee($subcommittee, ?string $orderBy = 'lastName'): array
	{

		$query = MemberRecord::find()->isActive()->onSubcommittee($subcommittee)->isRookie();

		if (!empty($orderBy))
		{
			$query = $query->orderBy($orderBy);
		}

		return $query->all();

	}

	/**
	 * @param bool $updateUserAttributes
	 *
	 * @return bool
	 */
	public function ensureAllActiveMembersAreUsers($updateUserAttributes = true)
	{

		$members = MemberRecord::find()->isActive()->all();

		$success = true;

		foreach ($members as $member)
		{
			try
			{
				$success = (bool) $this->ensureMemberIsUser($member, $updateUserAttributes) && $success;
			}
			catch(\Throwable $e)
			{
				CommitteeTools::error($e->getMessage());
				$success = false;
			}
		}

		return $success;

	}

	/**
	 * @param MemberRecord $member
	 * @param bool $updateUserAttributes
	 *
	 * @return bool
	 */
	public function ensureMemberIsUser(MemberRecord $member, $updateUserAttributes = true)
	{

		try
		{

			// Is there an existing User with this CN as a username?
			// Or maybe with this email?
			$user = Craft::$app->users->getUserByUsernameOrEmail($member->cn)
				?: Craft::$app->users->getUserByUsernameOrEmail($member->email);

			// If we found a User, and don't need to update, we're done.
			if ($user && !$updateUserAttributes)
			{
				return true;
			}

			// We need to continue, to create a User, to update one, or both.
			// Make sure we have a User in hand by creating one if none exists so far.
			if (empty($user))
			{
				$user = new User();
				$user->newPassword = Craft::$app->security->generateRandomString();
			}

			// It's a lot of typing... but let's save ourselves a query or two if everything is already correct.
			// (If we're dealing with a newly created User, these properties will be empty, i.e. not matching.)
			if (
				$user->username == $member->cn
				&& $user->email == $member->email
				&& $user->firstName == $member->firstName
				&& $user->lastName == $member->lastName
			)
			{
				return true;
			}

			$user->username = $member->cn;
			$user->email = $member->email;
			$user->firstName = $member->firstName;
			$user->lastName = $member->lastName;

			return Craft::$app->elements->saveElement($user);

		}
		catch(\Throwable $e)
		{
			CommitteeTools::error($e->getMessage());
			return false;
		}

	}

	/**
	 * Update User attributes from Member Records, matching on username or email.
	 */
	public function updateMemberUsers()
	{
		Craft::$app->getDb()->createCommand("update " . Table::USERS . " u inner join " . MemberRecord::tableName() . " m on m.email = u.email set u.username = m.cn, u.firstName = m.firstName, u.lastName = m.lastName")->execute();
		Craft::$app->getDb()->createCommand("update " . Table::USERS . " u inner join " . MemberRecord::tableName() . " m on m.cn = u.username set u.email = m.email, u.firstName = m.firstName, u.lastName = m.lastName")->execute();
	}

	/**
	 * @param $cn
	 *
	 * @return string|null
	 */
	public function fetchMemberPhotoData($cn)
	{

		try
		{

			$url = 'https://secure.rodeohouston.com/Membership/ChairtoolsRoster/RosterPhoto/' . $cn;

			$client = new Client();
			$response = $client->get($url)->getBody();

			$doc = new \DOMDocument();
			$doc->loadHTML($response, LIBXML_NOERROR);

			return $doc->getElementById('MemberPhotoUrl')->getAttribute('src');

		}
		catch(\Throwable $e)
		{
			return null;
		}

	}

	/**
	 * @param $cn
	 */
	public function saveMemberPhoto($cn)
	{

		$path = CommitteeTools::getInstance()->getSettings()->memberPhotosUploadPath;
		$filepath = $path . '/' . $cn . '.gif';
		$data = $this->fetchMemberPhotoData($cn);

		if (empty($data))
		{
			return;
		}

		$data = StringHelper::afterFirst($data, 'base64,', false);
		$data = base64_decode($data);

		if ($path)
		{
			try
			{
				FileHelper::createDirectory($path);
				FileHelper::writeToFile($filepath, $data);
			}
			catch (\Exception $e)
			{
				CommitteeTools::error("Could not write to {$filepath}");
			}
		}

	}

	public function updateMemberPhotos($cn = null)
	{

		$errorOccurred = false;

		$members = MemberRecord::find();
		$members = $cn ? $members->cn($cn)->all(): $members->isActive()->all();

		foreach ($members as $member)
		{
			try
			{
				$this->saveMemberPhoto($member->cn);
			}
			catch(\Throwable $e)
			{
				CommitteeTools::error($e->getMessage());
				$errorOccurred = true;
			}
		}

		if ($errorOccurred)
		{
			throw new \Exception("Some member photos were not updated.");
		}

	}

}
