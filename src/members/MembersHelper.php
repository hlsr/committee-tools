<?php
namespace hlsr\committeetools\members;

use craft\helpers\StringHelper;
use craft\helpers\UrlHelper;
use hlsr\committeetools\CommitteeTools;

class MembersHelper
{

	/**
	 * @param int|MemberRecord $member
	 */
	public static function normalizeMemberToCn($member = null): ?int
	{
		if (is_numeric($member))
		{
			return (int) $member;
		}
		if ($member instanceof MemberRecord)
		{
			return $member->cn;
		}
		return null;
	}

	public static function getPreferredFirstName(MemberRecord $member): string
	{
		return StringHelper::trim($member->preferredName ?: $member->firstName);
	}

	/**
	 * @param MemberRecord $member
	 *
	 * @return string
	 */
	public static function getFullPreferredName(MemberRecord $member): string
	{

		// TODO: Make all of this much more robust to corner cases.

		if (!empty($member->preferredName) && $member->preferredName != $member->lastName)
		{
			$firstName = StringHelper::trim($member->preferredName);
			/*
			 * Attempt to fix things if a member typed their name in all caps when registering...
			 * ...but also be wary of lowercasing initials.
			 */
			if (StringHelper::isUpperCase($firstName) && mb_strlen($firstName) > 3)
			{
				$firstName = StringHelper::titleize($firstName);
			}
		}
		else
		{
			$firstName = $member->firstName;
		}

		$name = $firstName;

		$lastName = $member->lastName;
		$lastWordInFirstName = trim(StringHelper::afterLast($firstName, ' ', false));

		/*
		 * Avoid duplication if a member put their last name in the preferred name field.
		 */
		if (empty($lastWordInFirstName) || !StringHelper::startsWith($lastName, $lastWordInFirstName))
		{
			$name .= ' ';
			$name .= $lastName;
		}

		return $name;

	}

	public function getLocalPhotoUrl(MemberRecord $member): string
	{
		$settings = CommitteeTools::getInstance()->getSettings();
		$path = StringHelper::ensureRight($settings->memberPhotosPublicPath, '/') . $member->cn . '.gif';
		return UrlHelper::url($path);
	}

}
