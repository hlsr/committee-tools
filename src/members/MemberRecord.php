<?php
namespace hlsr\committeetools\members;

use hlsr\committeetools\base\BaseRecord;
use hlsr\committeetools\CommitteeTools;
use hlsr\committeetools\rosters\Subcommittee;
use hlsr\committeetools\shifts\ShiftAssignmentRecord;
use hlsr\committeetools\shifts\ShiftAssignmentRequestRecord;
use hlsr\committeetools\shifts\ShiftElement;

/**
 * @property int $cn
 *
 * @property string $subcommitteeName1
 * @property string $subcommitteeName2
 * @property string $subcommitteeName3
 *
 * @property string $description
 * @property string $customerName
 * @property string $customerNamePhonetic
 * @property string $firstName
 * @property string $lastName
 * @property string $preferredName
 * @property bool $legalNameVerified
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 * @property string $phone
 * @property string $cellPhone
 * @property string $workPhone
 * @property string $email
 * @property bool $showDues
 * @property bool $commDues
 * @property bool $indemnityInd
 * @property bool $harassmentInd
 * @property bool $badgeReleased
 * @property null $badgeReleaseDate
 * @property bool $rookie
 * @property null $badgeIssueDate
 * @property string $badgePickupPerson
 * @property bool $ltcApplied
 * @property bool $inOtherCommittees
 *
 * @property string $photoUrl
 *
 * @property string $lightRosterTitle
 * @property string $lightRosterCustomerName
 * @property string $lightRosterCustomerNamePhonetic
 * @property string $lightRosterFirstName
 * @property string $lightRosterLastName
 * @property string $lightRosterCommitteeName
 * @property string $lightRosterAddress
 * @property string $lightRosterCityStateZip
 * @property string $lightRosterHomePhone
 * @property string $lightRosterCellPhone
 * @property string $lightRosterWorkPhone
 * @property string $lightRosterEmail
 * @property string $lightRosterSpouse
 * @property string $lightRosterEmployer
 * @property string $lightRosterJobTitle
 *
 * @property string $emergencyContactName
 * @property string $emergencyContactRelationship
 * @property string $emergencyContactTelephone
 * @property string $emergencyContactEmail
 *
 * @property string $status
 */
class MemberRecord extends BaseRecord
{

	const STATUS_ACTIVE = 'active';
	const STATUS_EMERITUS = 'emeritus';
	const STATUS_INACTIVE = 'inactive';
	const STATUS_WAITLIST = 'waitlist';

	const TableName = 'hlsr_committeetools_members';

	/*
	 * Public instance methods
	 */

	/**
	 * @return MemberQuery
	 */
	public static function find()
	{
		return new MemberQuery(static::class);
	}

	/**
	 * @return string
	 */
	public function getPreferredFirstName()
	{
		return MembersHelper::getPreferredFirstName($this);
	}

	/**
	 * @return string
	 */
	public function getFullPreferredName()
	{
		return MembersHelper::getFullPreferredName($this);
	}

	/**
	 * @return string
	 */
	public function getLocalPhotoUrl()
	{
		return MembersHelper::getLocalPhotoUrl($this);
	}

	/**
	 * @return Subcommittee|null
	 */
	public function getSubcommittee1()
	{
		return CommitteeTools::getInstance()->subcommittees->getSubcommittee($this->subcommitteeName1);
	}

	/**
	 * @return Subcommittee|null
	 */
	public function getSubcommittee2()
	{
		return CommitteeTools::getInstance()->subcommittees->getSubcommittee($this->subcommitteeName2);
	}

	/**
	 * @return Subcommittee|null
	 */
	public function getSubcommittee3()
	{
		return CommitteeTools::getInstance()->subcommittees->getSubcommittee($this->subcommitteeName3);
	}

	/**
	 * @return Subcommittee[]
	 */
	public function getSubcommittees()
	{
		return array_filter([
			$this->getSubcommittee1(),
			$this->getSubcommittee2(),
			$this->getSubcommittee3(),
		]);
	}

	public function getBestPhoneNumber()
	{
		if ($this->phone) return $this->phone;
		if ($this->cellPhone) return $this->cellPhone;
		if ($this->workPhone) return $this->workPhone;
		return null;
	}

	public function isActive()
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberIsActive))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberIsActive)($this);
		}
		return $this->status === self::STATUS_ACTIVE;
	}

	/**
	 * @return bool
	 */
	public function isCaptain()
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberIsCaptain))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberIsCaptain)($this);
		}
		return false;
	}

	public function isEligibleToPickUpShiftAssignments()
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberIsEligibleToPickUpShiftAssignments))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberIsEligibleToPickUpShiftAssignments)($this);
		}
		return false;
	}

	public function isEligibleToTradeShiftAssignments()
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberIsEligibleToTradeShiftAssignments))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberIsEligibleToTradeShiftAssignments)($this);
		}
		return false;
	}

	public function isManagement(): bool
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberIsManagement))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberIsManagement)($this);
		}
		return false;
	}

	public function isOnSubcommittee(Subcommittee $subcommittee): bool
	{
		return in_array(
			$subcommittee->name,
			[
				$this->subcommitteeName1,
				$this->subcommitteeName2,
				$this->subcommitteeName3,
			]
		);
	}

	/**
	 * Whether the member has paid their Show and Committee dues.
	 */
	public function isPaid(): bool
	{
		return ($this->showDues && $this->commDues);
	}

	/**
	 * Whether the member has completed all the requirements that make them eligible to work.
	 */
	public function isReadyToWork(): bool
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberIsReadyToWork))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberIsReadyToWork)($this);
		}
		return $this->isPaid()
			&& $this->legalNameVerified
			&& $this->indemnityInd;
	}

	/**
	 * Whether this member has permission to manage the given Shift
	 */
	public function canManageShift(ShiftElement $shift): bool
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberCanManageShift))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberCanManageShift)($this, $shift);
		}
		return $this->isManagement();
	}

	public function canPickUpShiftAssignment(ShiftAssignmentRecord $shiftAssignment)
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberCanPickUpShiftAssignment))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberCanPickUpShiftAssignment)($this, $shiftAssignment);
		}
		return false;
	}

	/**
	 * Whether this member has permission to resolve a given Shift Assignment Request
	 */
	public function canResolveShiftAssignmentRequest(ShiftAssignmentRequestRecord $request): bool
	{
		if (is_callable(CommitteeTools::getInstance()->getSettings()->memberCanResolveShiftAssignmentRequest))
		{
			return (CommitteeTools::getInstance()->getSettings()->memberCanResolveShiftAssignmentRequest)($this, $request);
		}
		return false;
	}

}
