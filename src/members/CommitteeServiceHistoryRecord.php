<?php
namespace hlsr\committeetools\members;

use hlsr\committeetools\base\BaseRecord;

class CommitteeServiceHistoryRecord extends BaseRecord
{

	const TableName = 'hlsr_committeetools_committee_service_history';

}
