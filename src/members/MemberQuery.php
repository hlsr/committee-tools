<?php
namespace hlsr\committeetools\members;

use craft\helpers\Db;
use hlsr\committeetools\rosters\Subcommittee;
use yii\db\ActiveQuery;

/**
 * @method MemberRecord[] all($db = null)
 */
class MemberQuery extends ActiveQuery
{

	/**
	 * @return MemberQuery
	 */
	public function cn($cn)
	{
		return $this->andWhere(Db::parseParam('cn', $cn));
	}

	public function description($description): self
	{
		return $this->andWhere(Db::parseParam('description', $description));
	}

	/**
	 * @return MemberQuery
	 */
	public function isActive()
	{
		return $this->andWhere(['status' => MemberRecord::STATUS_ACTIVE]);
	}

	/**
	 * Filter members by whether they have paid all their Show and Committee Dues
	 */
	public function isPaid(bool $paid = true)
	{
		if ($paid)
		{
			return $this->andWhere([
				'showDues' => 1,
				'commDues' => 1,
			]);
		}
		return $this->andWhere([
			'or',
			['showDues' => 0],
			['commDues' => 0],
		]);
	}

	/**
	 * @return MemberQuery
	 */
	public function isRookie()
	{
		return $this->andWhere(['rookie' => true]);
	}

	/**
	 * @param Subcommittee|string $subcommittee
	 */
	public function onSubcommittee($subcommittee): self
	{

		if ($subcommittee instanceof Subcommittee)
		{
			$subcommittee = $subcommittee->name;
		}

		$subcommittee = trim($subcommittee);

		if (empty($subcommittee))
		{
			return $this;
		}

		return $this
			->isActive()
			->andWhere([
				'or',
				['subcommitteeName1' => $subcommittee],
				['subcommitteeName2' => $subcommittee],
				['subcommitteeName3' => $subcommittee],
			]);

	}

}
