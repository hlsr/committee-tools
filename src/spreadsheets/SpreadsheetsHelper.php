<?php
namespace hlsr\committeetools\spreadsheets;

use PhpOffice\PhpSpreadsheet\IOFactory;

class SpreadsheetsHelper
{

	/**
	 * Parses a supplied file and returns its data as an array of rows.
	 *
	 * @param string $file
	 * @param bool $keyByHeaderRow
	 * @param bool $readDataOnly
	 *
	 * @return array
	 *
	 * @throws \PhpOffice\PhpSpreadsheet\Exception
	 * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
	 */
	public static function getRowsFromFile($file = null, $keyByHeaderRow = false, $readDataOnly = true): array
	{

		$reader = IOFactory::createReaderForFile($file);
		$reader->setReadDataOnly($readDataOnly);
		$spreadsheet = $reader->load($file);

		// Return the sheet data as an array
		$data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

		// Optionally use first row's values as keys
		if ($keyByHeaderRow)
		{
			$data = static::keyByHeaderRow($data);
		}

		return $data;

	}


	/**
	 * @param $array
	 * @param array $rename
	 * @return mixed
	 */
	public static function keyByHeaderRow($array, $rename = [])
	{

		$data = $array;
		$header = array_shift($data);

		foreach ($data as $rowKey => $row) {

			$data[$rowKey] = [];

			foreach ($row as $k => $v)
			{

				$newKey = isset($rename[$header[$k]]) ? $rename[$header[$k]] : $header[$k];
				if (!empty($newKey))
				{
					$data[$rowKey][$newKey] = $v;
				}

			}

		}

		return $data;

	}

}
