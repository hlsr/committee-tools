<?php
namespace hlsr\committeetools\web;

use craft\helpers\StringHelper;
use craft\helpers\UrlHelper;
use hlsr\committeetools\CommitteeTools;

class Router
{

	public function getUrl(string $path = '', $query = null)
	{

		$path = $this->getPrefixedRoute($path);
		$querySuffix = '';

		if ($query && !is_array($query))
		{
			$querySuffix = StringHelper::ensureLeft((string)$query, '/');
			$query = null;
		}

		return UrlHelper::url($path . $querySuffix, $query);

	}

	public function getPrefixedRoute(string $route): string
	{
		$base = CommitteeTools::getInstance()->getSettings()->routeBase;
		$base = $base ? StringHelper::ensureRight($base, '/') : '';
		return $base . trim($route);
	}

	public function getTemplateUrlRule(string $template): array
	{
		return [
			'route' => 'committee-tools/templates/render',
			'params' => [
				'template' => $template,
			]
		];
	}

	public function getSystemUrlRules(): array
	{
		return [

			// Admin

			$this->getPrefixedRoute('admin/import/assignments')
				=> $this->getTemplateUrlRule('admin/import/assignments'),

			// Members

			$this->getPrefixedRoute('members/<cn:\d+>')
				=> $this->getTemplateUrlRule('members/member'),

			$this->getPrefixedRoute('members/search')
				=> $this->getTemplateUrlRule('members/search'),

			// My Shifts

			$this->getPrefixedRoute('my-shifts')
				=> $this->getTemplateUrlRule('my-shifts'),

			// Reports

			$this->getPrefixedRoute('reports/shift-staffing')
				=> $this->getTemplateUrlRule('reports/shift-staffing'),

			$this->getPrefixedRoute('reports/shift-assignments-by-member')
			=> $this->getTemplateUrlRule('reports/shift-assignments-by-member'),

			// Shift Manager

			$this->getPrefixedRoute('shift-manager/all')
				=> $this->getTemplateUrlRule('shift-manager/all'),

			$this->getPrefixedRoute('shift-manager/today')
				=> $this->getTemplateUrlRule('shift-manager/today'),

			$this->getPrefixedRoute('shift-manager/yesterday')
				=> $this->getTemplateUrlRule('shift-manager/yesterday'),

			$this->getPrefixedRoute('shift-manager/shift-email-list/<shiftId:\d+>')
				=> $this->getTemplateUrlRule('shift-manager/shift-email-list'),

			$this->getPrefixedRoute('shift-manager/shift-roster/<shiftId:\d+>')
				=> $this->getTemplateUrlRule('shift-manager/shift-roster'),

			$this->getPrefixedRoute('shift-manager/shift-trades')
				=> $this->getTemplateUrlRule('shift-manager/shift-trades'),

			$this->getPrefixedRoute('shift-manager/print-shift-roster/<shiftId:\d+>')
				=> $this->getTemplateUrlRule('shift-manager/print-shift-roster'),

			// Shifts

			$this->getPrefixedRoute('shifts/create-trade-request')
				=> $this->getTemplateUrlRule('shifts/create-trade-request'),

			$this->getPrefixedRoute('shifts/edit-wishlist')
				=> $this->getTemplateUrlRule('shifts/edit-wishlist'),

			$this->getPrefixedRoute('shifts/pick-up-assignment')
				=> $this->getTemplateUrlRule('shifts/pick-up-assignment'),

			$this->getPrefixedRoute('shifts/search-pickup')
				=> $this->getTemplateUrlRule('shifts/search-pickup'),

			$this->getPrefixedRoute('shifts/search-trades')
				=> $this->getTemplateUrlRule('shifts/search-trades'),

			// Shifts

			$this->getPrefixedRoute('sms')
				=> $this->getTemplateUrlRule('sms/send'),

			// Success

			$this->getPrefixedRoute('success')
				=> $this->getTemplateUrlRule('success'),

		];
	}

}
