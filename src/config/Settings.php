<?php
namespace hlsr\committeetools\config;

use craft\base\Model;
use craft\helpers\ConfigHelper;
use yii\base\InvalidConfigException;

class Settings extends Model
{

	/*
	 * System setup
	 */

	/**
	 * @var int
	 */
	public $showYear;

	/**
	 * @var string
	 */
	public $siteLabel = "Committee Tools";

	/**
	 * @var string
	 */
	public $idLabel = "HLSR-ID";

	/**
	 * @var string
	 */
	public $routeBase = 'ct';

	/**
	 * @var string
	 */
	public $customTemplatePrefix;

	/**
	 * @var string
	 */
	public $systemTemplateRoot = '__ct';

	/**
	 * @var string
	 */
	public $memberPhotosUploadPath;

	/**
	 * @var string
	 */
	public $memberPhotosPublicPath;

	/*
	 * Subcommittees
	 */

	/**
	 * @var array
	 */
	public $subcommitteeAbbreviations = [];

	/**
	 * @var array
	 */
	public $subcommitteeDisplayNames = [];

	/**
	 * @var array
	 */
	public $subcommitteeNicknames = [];

	/*
	 * Member permissions
	 */

	/**
	 * @var callable
	 */
	public $memberIsActive;

	/**
	 * @var callable
	 */
	public $memberIsCaptain;

	/**
	 * @var callable
	 */
	public $memberIsManagement;

	/**
	 * @var callable
	 */
	public $memberIsEligibleToPickUpShiftAssignments;

	/**
	 * @var callable
	 */
	public $memberIsEligibleToTradeShiftAssignments;

	/**
	 * @var callable
	 */
	public $memberIsReadyToWork;

	/**
	 * @var callable
	 */
	public $memberCanManageShift;

	/**
	 * @var callable
	 */
	public $memberCanPickUpShiftAssignment;

	/**
	 * @var callable
	 */
	public $memberCanResolveShiftAssignmentRequest;

	/*
	 * Admin functions
	 */

	/**
	 * @var string
	 */
	public $rostersUploadPath;

	/**
	 * @var string
	 */
	public $shiftsUploadPath;

	/*
	 * Shift Assignments
	 */

	/**
	 * @var callable
	 */
	public $getAssignmentAreaOptions;

	/*
	 * Shift Assignment Requests
	 */

	/**
	 * @var callable
	 */
	public $getTradeEligibleAssignments;

	/**
	 * @var callable
	 */
	public $getPickupEligibleAssignments;

	/**
	 * @var int
	 */
	public $tradeRequestExpiryDuration = 'P1D';

	/**
	 * @var int
	 */
	public $pickupRequestExpiryDuration = 'P1D';

	/*
	 * Shift Wishlists
	 */

	/**
	 * @var callable
	 */
	public $getWishlistEligibleShifts;

	/*
	 * Notifications
	 */

	/**
	 * @var string
	 */
	public $twilioSid;

	/**
	 * @var string
	 */
	public $twilioToken;

	/**
	 * @var string
	 */
	public $twilioFromNumber;

	/**
	 * @var string
	 */
	public $smsPrefix;

	/**
	 * @var bool
	 */
	public $silenceSmsNotifications = false;

	/**
	 * @var array
	 */
	public $testSmsRecipients;

	/**
	 * @var string
	 */
	public $emailSubjectPrefix;

	/**
	 * @var bool
	 */
	public $silenceEmailNotifications = false;

	/**
	 * @var array
	 */
	public $testEmailRecipients;

	/**
	 * @var array
	 */
	public $bccEmailRecipients;

	/*
	 * Search
	 */

	/**
	 * @var string
	 */
	public $algoliaApplicationId;

	/**
	 * @var string
	 */
	public $algoliaAdminApiKey;

	/**
	 * @var string
	 */
	public $algoliaPublicApiKey;

	/**
	 * @var string
	 */
	public $membersSearchIndexName;

	/*
	 * Entry notifications
	 */

	/**
	 * @var callable
	 */
	public $entryAfterPropagateHandler;

	/*
	 *
	 */

	/**
	 * @throws InvalidConfigException
	 */
	public function init()
	{
		// Normalize durations to seconds.
		$this->tradeRequestExpiryDuration = ConfigHelper::durationInSeconds($this->tradeRequestExpiryDuration);
		$this->pickupRequestExpiryDuration = ConfigHelper::durationInSeconds($this->pickupRequestExpiryDuration);
	}

}
